/* Digit calculator v1.0
   by Mircea
   idea from codewars.com
*/

interface Calculable {
    double calculate(double a, double b);
}

public class Calculator {


    public static void main(String[] args) {
        double x = 9;
        double y = 5;

        /*Lambda statment*/
        Calculable addition = (a , b) -> {return a + b;};

        /*lambda expression*/
        Calculable substraction = (a, b) -> a - b;

        /*Anonymous method override*/
        Calculable multiply = new Calculable() {
            @Override
            public double calculate(double a, double b) {
                return a * b;
            }
        };

        Calculable division = (a, b) -> a / b;

        Calculable modulus = (a , b) -> a % b;

        System.out.println(addition.calculate(x, y));
        System.out.println(substraction.calculate(x, y));
        System.out.println(multiply.calculate(x, y));
        System.out.println(division.calculate(x, y));
        System.out.println(modulus.calculate(x, y));
    }

}