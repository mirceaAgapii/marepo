import java.io.*;
//  geting the lowest and the highest numbers from a String entered from the keyboard separeted by space
public class MinMax {
    public static String highAndLow(String numbers) {
        int max, min, temp = 0;
        String result = "";
        String[] numbersArr = numbers.split(" ");
        int[] arr = new int[numbersArr.length];

        //converting String array to int
        for (int i = 0; i < numbersArr.length; i++) {
            arr[i] = Integer.parseInt(numbersArr[i]);
        }

        //get the lowest number
        for (int i = 1; i < arr.length; i++) {
            if(arr[0] > arr[i]) {
                temp = arr[0];
                arr[0] = arr[i];
                arr[i] = temp;
            }
        }
        min = arr[0];

        //get the highest number
        for (int i = 1; i < arr.length; i++) {
            if(arr[0] < arr[i]) {
                temp = arr[0];
                arr[0] = arr[i];
                arr[i] = temp;
            }
        }

        max = arr[0];

        result =  "The highest numbers is " + Integer.toString(max) + "; The lowest numbers is " + Integer.toString(min) + ";";
        return result;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String arr = reader.readLine();
        System.out.println(highAndLow(arr));
    }
}