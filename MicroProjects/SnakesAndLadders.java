/*This is a Java version of table game Snakes and Ladders v1.0
 *by Mircea
 *idea from codewars.com
 *
 *The idea of this game is to go from the start square(0 index of the array) to the last one(last index of the array) 
 *in a limited number of throws of the dice,
 *on the way we can skip some squares forward by using the ladders, which in our case are positive numbers in 
 *board array, or we will have to go back several squares if we get on a snake, which are negative numbers.
*/

public class SnakesAndLadders {
  static int count = 0;

  public static int snakesAndLadders(final int[] board, final int[] dice) {
    int currentPosition = 0;                                            //our beginnig position is on 0 index
    
    for (int i = 0; i < dice.length; i++) {				//loop through the dice array like we're throwing the dice
      if(currentPosition == (board.length - 1)) return currentPosition; //verify if the current position is on the last square, in this case we won
      SnakesAndLadders.count++;						//counting the number of throws
      if((currentPosition + dice[i]) > (board.length - 1)) continue;    //verify if the next position is in the array, if not we are not moving anywhere
      currentPosition += dice[i];					//moving forward by the number on the dice
      currentPosition += board[currentPosition];			//if there are any snakes or ladders we'll move back or forward, if there is nothing 
									//we'll stay on that place
    }
    return currentPosition;
  }
  
  public static void main(String[] args) {
	final int dice[] = {2,1,5,1,5,4};
     	final int board[] = {0,0,3,0,0,0,0,-2,0,0,0};
      
      	System.out.println("You ended the game on " + snakesAndLadders(board, dice) + " square. You have made " + SnakesAndLadders.count + " throws.");
  }
}