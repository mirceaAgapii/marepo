/*Rock Papper Scissors game v2.0 User vs Artificial Intelect (AI) aka Math.random xD
  by Mircea
*/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RPS {

    private static void rockPaperScissors(String userChoice) {                  
        int pc = (int)Math.ceil(Math.random() * 3);                                                             //generating a random number for the AI choice
        int user = Integer.parseInt(userChoice);

        System.out.println();
        if(user == 1) {
            System.out.println("You choosed Rock");                                                            //printing to the terminal your choice
        }
        if(user == 2) {
            System.out.println("You choosed Papper");
        }
        if(user == 3) {
            System.out.println("You choosed Scissors");
        }

        if(pc == 1) { 
            System.out.println("The AI choosed Rock");                                                         //printing to the terminal the AI choice
        }
        if(pc == 2) {
            System.out.println("The AI choosed Papper");
        }
        if(pc == 3) {
            System.out.println("The AI choosed Scissors");
        }

        System.out.println();

        if((pc == 1 && user == 1) || (pc == 2 && user == 2) || (pc == 3 && user == 3)) {
            System.out.println("Draw");
        }   else if((pc == 1 && user == 2) || (pc == 2 && user == 3) || (pc == 3 && user == 1)) {
                System.out.println("You Won! Congrats!");
        }   else {
                System.out.println("You Lose =(");
        }
    }

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String userChoice;

        try {
            while (true) {
                System.out.println("You want to play a new game? Y / N");                                       //starting the game, Y/y to continue, 
                String answer = reader.readLine();                                                           

                if(answer.equals("Y") || answer.equals("y")) {
                    System.out.println();
                    System.out.println("Please enter your choice:");
                    System.out.println("1 for Rock");
                    System.out.println("2 for Papper");
                    System.out.println("3 for Scissors");

                    while (true) {
                        userChoice = reader.readLine();                                                         //entering the users choice
                        if (userChoice.equals("1") || userChoice.equals("2") || userChoice.equals("3")) {
                            rockPaperScissors(userChoice);
                            break;
                        } else {
                            System.out.println("Please enter a valid option");
                            System.out.println();
                        }
                    }
                    System.out.println("-----------------------------------");
                    System.out.println();
                    System.out.println();
                }   else if(answer.equals("N") || answer.equals("n")) {                                         //N/n to finish the game
                        break;
                }   else {
                        System.out.println("Please enter a valid option");
                }
            }
        } catch (IOException e) {                                                                              //handeling the IOException which will not happen
            throw new RuntimeException();
        }
    }
}