/**
*Searchimg for doubles in arrays
*Maximum 4 pairs
*/

public class ArrayTest {
  public Solution solution;
  
  public ArrayTest() {
  	this.solution = new Solution();
  }
  
  void checkDouble(int[] arr) {
  	loop:
    for(int i = 0; i <= arr.length - 2; i++) {
      for(int j = i + 1; j <= arr.length - 1; j++) {
        if(arr[i] == arr[j]) {
          if(this.solution.firstPair == null) {
            this.solution.firstPair = new int[3];
            this.solution.firstPair[0] = arr[i];
            this.solution.firstPair[1] = i;
            this.solution.firstPair[2] = j;
          } else if(this.solution.secondPair == null) {
            	this.solution.secondPair = new int[3];
            	this.solution.secondPair[0] = arr[i];
            	this.solution.secondPair[1] = i;
            	this.solution.secondPair[2] = j;
          } else if(this.solution.thirdPair == null) {
            	this.solution.thirdPair = new int[3];
            	this.solution.thirdPair[0] = arr[i];
            	this.solution.thirdPair[1] = i;
            	this.solution.thirdPair[2] = j;
          } else if(this.solution.fourthPair == null) {
            	this.solution.fourthPair = new int[3];
            	this.solution.fourthPair[0] = arr[i];
            	this.solution.fourthPair[1] = i;
            	this.solution.fourthPair[2] = j;
          } else {
          		System.out.println("No more space for pairs");
                break loop;
          }
          break;
        }
      }
    }
  }
  
  public static void main(String[] args) {
    ArrayTest doubleTest = new ArrayTest();
    int array[] = {5, 0, 632, 8, 4, 2, 4, 16, 15, 115, 5};
	
    doubleTest.checkDouble(array);
    
    if(doubleTest.solution.firstPair != null) {
    	System.out.println(doubleTest.solution.printArr(doubleTest.solution.firstPair));
    }
    if(doubleTest.solution.secondPair != null) {
    	System.out.println(doubleTest.solution.printArr(doubleTest.solution.secondPair));
    }
    if(doubleTest.solution.thirdPair != null) {
    	System.out.println(doubleTest.solution.printArr(doubleTest.solution.thirdPair));
    }
    if(doubleTest.solution.fourthPair != null) {
    	System.out.println(doubleTest.solution.printArr(doubleTest.solution.fourthPair));
    }
  }
  
}

public class Solution {
  public int[] firstPair;
  public int[] secondPair;
  public int[] thirdPair;
  public int[] fourthPair;
  
  public static String printArr(int[] arr) {
  	return "Pair of " + arr[0] + "'s on indexes " + arr[1] + " and " + arr[2];
  }
}