import java.lang.Math.*;

public class ArrayTest {
  
  int searchArrayIndex(int[] array, int n) { 
    int index = -1;
    
    for(int i = 0; i < array.length; i++) {
    	if(array[i] == n) {
        	index = i;
          	break;
     	}	
    }
    
    return index;
  }
  public static void main(String[] args) {
    int[] array = new int[10];
    int indexOf = (int)Math.ceil(Math.random() * 10);
	
    for(int i = 0; i < 10; i++) {
    	array[i] = (int)Math.ceil(Math.random() * (100 + 100) - 100);	
    }
    
    System.out.println("The number to search is " + array[indexOf] + ", in the following array:");
    
    for(int i: array) {
    	System.out.print(i + " ");
    }
    
    ArrayTest indexObj = new ArrayTest();
    System.out.println("\nThe index of the searched number is " + indexObj.searchArrayIndex(array, array[indexOf]));
  }
  
}