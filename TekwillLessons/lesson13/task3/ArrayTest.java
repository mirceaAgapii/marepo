public class ArrayTest {
  
  int[] reverseArray(int[] arr1) {
  
  
  	int[] arr2 = new int[arr1.length];
    
    for(int i = 0; i < arr1.length; i++) {
    	arr2[arr1.length - (1 + i)] = arr1[i];
    }
    
    return arr2;
  }
  
  public static void main(String[] args) {
    ArrayTest reverseTest = new ArrayTest();
    int[] arr = {1, 2, 3, 4, 5};
    
    if(arr.length != 0) {
      for(int i: reverseTest.reverseArray(arr)) {
          System.out.print(i + " ");
      }
    } else {
    	System.out.println("There is nothing to reverse, the array is empty");
    }
  }
  
}