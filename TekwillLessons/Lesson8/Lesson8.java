import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lesson8 {

    private static void summAllNumbers(int n) {
        int sum = 0;

        for (int i = 1; i < n; i++) {
            sum += i;
        }

        System.out.println("The sum of the figures from 0 to " + n + " equals to " + sum);
    }
    
    private static void evenNumbers(int n) {
        System.out.println("Even numbers from 0 to " + n);

        for (int i = 0; i < n; i++) {
            if(i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
    }

    private static void oddNumbers(int n) {
        System.out.println("Odd numbers from 0 to " + n);

        for (int i = 0; i < n; i++) {
            if(i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
    }

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int opt = 0;
        int n = 0;
        String exp;

        System.out.println("Please enter a number, N");
        while (true) {
            try {
                n = Integer.parseInt(reader.readLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("This is not a number");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        System.out.println("Please choose your desired task:");
        System.out.println("1. Sum all the numbers from 0 to N");
        System.out.println("2. Display all even numbers from 0 to N");
        System.out.println("3. Display all odd numbers from 0 to N");
        System.out.println("Press X to exit");
        try {
            while (true) {
                exp = reader.readLine();

                if (exp.equals("1") || exp.equals("2") || exp.equals("3")) {
                    opt = Integer.parseInt(exp);
                    break;
                } else if(exp.equals("X") || exp.equals("x")) {
                    System.out.println("Good bye!");
                    break;
                } else {
                    System.out.println("Please enter a valid option");
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("This will not happen", e);
        }

        switch(opt) {
            case 1:
                summAllNumbers(n);
                break;
            case 2:
                evenNumbers(n);
                break;
            case 3:
                oddNumbers(n);
                break;
        }
    }
}
