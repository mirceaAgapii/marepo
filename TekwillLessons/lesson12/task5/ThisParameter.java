package lesson12.task5;
/**
 * Task 5
 * One class with a method that has a parameter.
 * When calling the method, it should be possible to pass "this" as parameter.
 */
public class ThisParameter {
    String str;

    public ThisParameter(String str) {
        this.str = str;
        this.printString(this);
    }

    private void printString(ThisParameter obj) {
        System.out.println(this.str);
    }

    public static void main(String[] args) {
        ThisParameter obj = new ThisParameter("This method works");

    }
}
