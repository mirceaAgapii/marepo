package lesson12.task7;
/**
 * Task 7
 * One class with a static and non static blocks.
 * In the blocks put some logic.
 * Make an object and check the initialization order.
 * */
public class Blocks {
    static int count;

    public Blocks() {
        System.out.println("Object number " + count + " created \n");
    }

    static
    {
        System.out.println("Some logic to be executed first when programs  \n" +
                           "This logic will be executed once \n" +
                           "Before every other logic \n");
    }

    {
        count++;
        System.out.println("Some logic to be executed every time a new object is created");
    }

    public static void main(String[] args) {
        System.out.println("First statement in main \n");
        Blocks block1 = new Blocks();
        Blocks block2 = new Blocks();
        Blocks block3 = new Blocks();
        Blocks block4 = new Blocks();

    }
}
