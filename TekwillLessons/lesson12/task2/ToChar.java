package lesson12.task2;
/**
 * Task 2
 * One class with 3 overloaded methods.
 */
public class ToChar {

    char toChar(int c) {
        char sym = (char)c;
        return sym;
    }

    char toChar(String str) {
        char sym = str.charAt(0);
        return sym;
    }

    char toChar() {
        char c = (char)Math.round(Math.random()*100);
        return c;
    }

    public static void main(String[] args) {
        ToChar first = new ToChar();
        System.out.println(first.toChar());
        System.out.println(first.toChar(60));
        System.out.println(first.toChar("M"));
    }
}
