package lesson12.task3and4;
/**
 * Task 3 and Task 4
 * One class with three overloaded constructors.
 * Use "this" to assign the passed parameters to class properties.
 * One class with three overloaded constructors using "this()".
 * Use "this" to assign the passed parameters to class properties.
 */
public class Person {
    String name;
    int age;
    char gender;

    public Person(String name) {
        this.name = name;
        age = 30;
    }

    public Person(String name, int age) {
        this(name);
        this.age = age;
    }

    public Person(String name, int age, char gender) {
        this(name, age);
        this.gender = gender;
    }

    public static void main(String[] args) {
        Person person = new Person("Mircea", 27, 'm');
    }
}
