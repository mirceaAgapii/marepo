package lesson12.task1;
/**
 * Task 1
 * One class with two or three methods.
 * In the first method call the second and third method with "this" keyword.
 */
public class Mathematics {
    int solution;

    public int calculation(int a, int b, char c) {
        switch (c) {
            case 'a':
                solution = this.add(a, b);
                break;
            case 's':
                solution = this.subtract(a, b);
                break;
            case 'm':
                solution = this.multiply(a, b);
                break;
            case 'd':
                solution = this.divide(a, b);
                break;
        }
        return solution;
    }

    private int add(int a, int b) {
        return a + b;
    }

    private int subtract(int a, int b) {
        return a - b;
    }

    private int multiply(int a, int b) {
        return a * b;
    }

    private int divide(int a, int b) {
        return a / b;
    }

    public static void main(String[] args) {
        int a = 10;
        int b = 5;
        char c = 'm';

        Mathematics firstExample = new Mathematics();
        System.out.println(firstExample.calculation(a, b, c));
    }
}
