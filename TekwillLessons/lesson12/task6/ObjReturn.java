package lesson12.task6;
/**
 * Task 6
 * One class with a method or two that return a type of the current class. Hint: using "this".
 *
 * Not sure if I understood how to solve this task properly
 * I hope to get some hints at next lesson
 **/
public class ObjReturn {
    ObjReturn getInstance() {
        return this;
    }

    public static void main(String[] args) {
        ObjReturn obj = new ObjReturn();

        ObjReturn obj2 = obj.getInstance();

        System.out.println(obj);
        System.out.println(obj2);
    }
}
