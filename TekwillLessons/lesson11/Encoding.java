package lesson11;

import lesson11.task3.AccessStringMethods;
import java.io.IOException;

public class Encoding {
    public static void main(String[] args) throws IOException {
        AccessStringMethods test = new AccessStringMethods();

        System.out.println("Encoded message: " + "\"" + test.encryptingAMessage() + "\"");
    }
}
