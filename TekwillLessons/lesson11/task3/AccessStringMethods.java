package lesson11.task3;

import lesson11.task2.TestingStringMethods;

import java.io.IOException;

public class AccessStringMethods {
    TestingStringMethods testStr;

    public AccessStringMethods() {
        this.testStr = new TestingStringMethods();
    }

    public String encryptingAMessage() throws IOException {
        return this.testStr.encrypt();
    }

    public String decryptingAEncodedMessage(String encodedMessage) {
        return this.testStr.decrypt(encodedMessage);
    }

    public String toCamelCaseMethod(String someString) {
        return this.testStr.toCamelCase(someString);
    }
}
