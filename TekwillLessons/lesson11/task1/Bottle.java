
package lesson11.task1;

public class Bottle {
    private boolean isFull;
    private boolean isEmpty;
    private int maxCapacity;
    private int currentVolume;
    private String content;
    private static int count;

    public Bottle(int maxCapacity, int currentVolume, String content) {
        this.maxCapacity = maxCapacity;
        this.currentVolume = currentVolume;
        this.content = content;
    }

    public Bottle(int maxCapacity, int currentVolume, String content, boolean isEmpty) {
        this(maxCapacity, currentVolume, content);
        this.isFull = false;
        this.isEmpty = isEmpty;

        System.out.println(this.toString());

        count++;
    }

    public Bottle(boolean isFull, int maxCapacity, int currentVolume, String content) {
        this(maxCapacity, currentVolume, content);
        this.isFull = isFull;
        this.isEmpty = false;

        System.out.println(this.toString());

        count++;
    }

    public static int getCount() {
        return count;
    }

    private boolean checkIfFull() {
        if (currentVolume == maxCapacity) {
            isFull = true;
        } else {
            isFull = false;
        }
        return isFull;
    }

    private boolean checkIfEmpty() {
        if (currentVolume == 0) {
            isEmpty = true;
        } else {
            isEmpty = false;
        }
        return isEmpty;
    }

    public void addContent(int volume) {
        if (checkIfFull()) {
            System.out.println("Can not add any additional content. The bottle is full, sorry");
        } else {
            if ((volume + currentVolume) <= maxCapacity) {
                currentVolume += volume;
                System.out.println("The content was added to the bottle. Current volume is " + currentVolume);
            } else {
                System.out.println("Amount that you are about to add is to much, you can add only " + (maxCapacity - currentVolume));
                currentVolume += (maxCapacity - currentVolume);
            }
        }
    }

    public void emptyTheBottle() {
        if (checkIfEmpty()) {
            System.out.println("The bottle is empty, you don't have anything to pour out");
        } else {
            System.out.println("You have emptied the bottle by " + currentVolume + "ml of " + content);
            currentVolume = 0;
        }
    }

    @Override
    public String toString() {
        return "This bottle with " + content + ", has " + maxCapacity +
                " ml of total volume, current volume of " + content.toLowerCase() +
                " is " + currentVolume + " ml";
    }
}
