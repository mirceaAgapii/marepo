package lesson11;

import lesson11.task3.AccessStringMethods;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Decoding {
    public static void main(String[] args) throws IOException {
        AccessStringMethods test = new AccessStringMethods();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter your encoded message:");
        String encodedMessage = reader.readLine();

        System.out.println("Decoded message: " + "\"" + test.decryptingAEncodedMessage(encodedMessage) + "\"");

    }
}
