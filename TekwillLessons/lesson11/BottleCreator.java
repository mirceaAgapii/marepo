package lesson11;

import lesson11.task1.Bottle;

public class BottleCreator {
    public static void main(String[] args) {

        //Creating bottles
        System.out.println();
        System.out.println();
        Bottle bottleWithWater = new Bottle(1000, 300, "Water", false);
        bottleWithWater.addContent(500);
        Bottle bottleWithCola = new Bottle(true, 330, 330, "Cola");
        bottleWithCola.emptyTheBottle();

        System.out.println("We have created " + Bottle.getCount() + " bottles");
    }
}
