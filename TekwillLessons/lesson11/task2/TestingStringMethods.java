package lesson11.task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestingStringMethods {
/** Encrypt
 */
    public String encrypt() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter your message to be encrypted:");

        String text = reader.readLine();
        int levelOfEncryption = text.length();

        if (levelOfEncryption % 2 == 0) {
            levelOfEncryption++;
        }

        String message1 = "";
        String message2 = "";
        String finalMessage = text;

        if(levelOfEncryption <= 0) return finalMessage;

        for (int j = 1; j <= levelOfEncryption; j++) {
            message1 = "";
            message2 = "";
            for (int i = 1; i < finalMessage.length(); i += 2) {
                message1 += finalMessage.charAt(i);
                message2 += finalMessage.charAt(i - 1);
            }

            if (finalMessage.length() % 2 != 0) {
                message2 += finalMessage.charAt(finalMessage.length() - 1);
            }
            finalMessage = message1 + message2;
        }
        return finalMessage;
    }
/** Decrypt
 */
    public String decrypt(String encryptedText) {
        int levelOfEncryption = encryptedText.length();

        if (levelOfEncryption % 2 == 0) {
            levelOfEncryption++;
        }

        String finalMessage = "";
        String message = encryptedText;
        int strLength = 0;
        int strLength2 = 0;

        if (levelOfEncryption <= 0) return encryptedText;

        if (encryptedText.length() % 2 != 0) {
            strLength = (int)Math.floor(encryptedText.length() / 2);
            strLength2 = strLength + 1;
        } else {
            strLength = encryptedText.length() / 2;
            strLength2 = strLength;
        }

        for (int j = 1; j <= levelOfEncryption; j++) {
            finalMessage = "";
            for(int i = 0; i < strLength; i++) {
                finalMessage += message.charAt(i + strLength);
                finalMessage += message.charAt(i);
            }
            if (message.length() % 2 != 0) {
                finalMessage += message.charAt(message.length() - 1);
            }
            message = finalMessage;
        }

        return finalMessage;
    }

/** To camel case method
 */
    public String toCamelCase(String string) {
        if (string.length() == 0) return "";

        String result = "";
        String[] arr = string.trim().split("\\s+");

        result += arr[0].toLowerCase();
        for(int i = 1; i < arr.length; i++) {
            result += arr[i].substring(0, 1).toUpperCase() + arr[i].substring(1).toLowerCase();
        }

        return result;
    }
}
