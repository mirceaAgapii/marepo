package Lesson9.Task1;

public class Exercises {
    protected static void summAllNumbers(int n) {
        int sum = 0;

        for (int i = 1; i < n; i++) {
            sum += i;
        }

        System.out.println("The sum of the figures from 0 to " + n + " equals to " + sum);
    }

    protected static void evenNumbers(int n) {
        System.out.println("Even numbers from 0 to " + n);

        for (int i = 0; i < n; i++) {
            if(i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
    }

    protected static void oddNumbers(int n) {
        System.out.println("Odd numbers from 0 to " + n);

        for (int i = 0; i < n; i++) {
            if(i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
    }
}
