package Lesson9.Task2;

public class Pet extends PetClinic{
    private String petName;
    private boolean hasOwner;
    private int petAge;
    private char petGender;

    /*Pet has owner*/
    protected Pet(int id, String petKind, String petName, int petAge, char petGender) {
        super(id, petKind);
        this.petName = petName;
        this.hasOwner = true;
        this.petAge = petAge;
        this.petGender = petGender;
    }

    /*Pet from the street*/
    protected Pet(int id, String petKind, char petGender) {
        super(id, petKind);
        this.petName = "unknown";
        this.hasOwner = false;
        this.petAge = 0;
        this.petGender = petGender;
    }

    protected void petRename(String petName) {
        this.petName = petName;
    }

    protected void petChangeKind(String petKind) {
        this.petKind = petKind;
    }

    @Override
    public String toString() {
        return "Clinic.Pet number " + this.id + ", of kind - " + this.petKind + ", with name " + this.petName
                + " and age of " + this.petAge;
    }

    /*Char to int meth*/
    protected int petGenderToInteger() {
        System.out.println("This is " + this.petName + ", it's gender is " + this.petGender);
        return (int)this.petGender;
    }
}
