package Lesson9.Task2;

public class PetClinic {
    protected int id;
    protected String petKind;
    private static int petCount;

    /*Constructor*/
    public PetClinic(int id, String petKind) {
        this.id = id;
        this.petKind = petKind;
        petCount++;
    }

    protected static int getPetCount() {
        return petCount;
    }

}
