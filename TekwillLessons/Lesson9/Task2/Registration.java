package Lesson9.Task2;

public class Registration extends Pet {
    protected Registration(int id, String petKind, String petName, int petAge, char petGender) {
        super(id, petKind, petName, petAge, petGender);
    }

    protected Registration(int id, String petKind, char petGender) {
        super(id, petKind, petGender);
    }

    public static void main(String[] args) {
        Pet cat1 = new Pet(1, "cat", "Garfield", 3, 'm');
        System.out.println(cat1);
        cat1.petRename("Kotea");
        System.out.println(cat1);

        Pet dog1 = new Pet(2, "dog", "Sharik", 5, 'm');
        System.out.println(dog1);

        Pet cat2 = new Pet(3, "cat", 'f');
        System.out.println(cat2);

        System.out.println("Number of pets in the clinic: " + getPetCount());

        System.out.println("Pets gender in Integer will be " + cat1.petGenderToInteger());
        System.out.println("Pets gender in Integer will be " + cat2.petGenderToInteger());
    }
}
