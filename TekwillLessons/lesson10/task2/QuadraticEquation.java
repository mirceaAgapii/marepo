package lesson10.task2;

public class QuadraticEquation  {

    public static void quadr(int a, int b, int c) {
        if (a == 0) {
            System.out.println("This is not a Quadratic equation");
        }

        if (delta(a, b, c) < 0) {
            System.out.println("The equation has no solutions");
        } else if (delta(a, b, c) == 0) {
            System.out.println("The equation has two solutions and they are both equal to: " );
            QuadrSolutions solution = discriminant(a, b);
            System.out.println("1. " + solution.getFirstSolution());
        } else {
            System.out.println("The equation has two diferent solutions: ");
            QuadrSolutions solutions = discriminant(a, b, delta(a, b, c));
            System.out.println("1. " + solutions.getFirstSolution());
            System.out.println("2. " + solutions.getSecondSolution());
        }
    }

    private static int delta(int a, int b, int c) {
        return (b * b) - 4 * a * c;
    }

    private static QuadrSolutions discriminant(int a, int b) {
        return new QuadrSolutions(-b / (2 * a));
    }

    private static QuadrSolutions discriminant(int a, int b, int delta) {
        return new QuadrSolutions((-b + Math.sqrt(delta)) / (2 * a),
                                  (-b - Math.sqrt(delta)) / (2 * a));
    }
}
