package lesson10.task2;

public class QuadrSolutions {
    private double firstSolution;
    private double secondSolution;

    protected QuadrSolutions(double first) {
        firstSolution = first;
    }

    protected QuadrSolutions(double first, double second) {
        firstSolution = first;
        secondSolution = second;
    }

    protected double getFirstSolution(){
        return firstSolution;
    }

    protected double getSecondSolution(){
        return secondSolution;
    }
}
