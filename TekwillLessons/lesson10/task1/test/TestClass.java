package lesson10.task1.test;

import lesson10.task1.first.Avg;
import lesson10.task1.second.Ascend;
import lesson10.task1.third.RandomName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class TestClass {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Random rand = new Random();
        int n;
        int[] array;

        System.out.println("Please insert the length of the array");
        while (true) {
            try {
                n = Integer.parseInt(reader.readLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("This is not a number");
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
        array = new int[n];

        System.out.println("Plase insert integers in your array");
        for (int i = 0; i < n; i++) {
            try {
                array[i] = Integer.parseInt(reader.readLine());
            } catch (NumberFormatException e) {
                System.out.println("Wrong number, we will add a random number");
                array[i] = rand.nextInt(100 + 100) - 100;
            } catch (IOException e) {
                throw new RuntimeException();
            }
            System.out.println(("Array[" + i + "] = " + array[i]));
        }

        Avg avgObj = new Avg();
        System.out.println("The average of your array is " + avgObj.averageOfArr(array));

        Ascend ascendObj = new Ascend();
        ascendObj.sortAscending(array);

        RandomName name = new RandomName("Mircea");
        name.showName();
    }
}
