package lesson10.task1.second;

public class Ascend {

    public void sortAscending(int[] arr) {
        int temp = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[i] > arr[j]) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        System.out.print("Array sorted ascending: ");
        for (int i: arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
