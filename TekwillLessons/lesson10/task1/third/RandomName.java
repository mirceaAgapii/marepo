package lesson10.task1.third;

public class RandomName {
    private String name;

    public RandomName(String name) {
        this.name = name;
    }

     public void showName() {
        System.out.println(name);
    }

}
