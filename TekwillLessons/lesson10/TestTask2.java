package lesson10;

import lesson10.task2.QuadraticEquation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestTask2 {

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = 0;
        int b = 0;
        int c = 0;

        System.out.println("Please enter your quadratic equation in the following format:");
        System.out.println("ax^2 + bx + c = 0");
        while(true) {
            try {
                System.out.println("a = ");
                a = Integer.parseInt(reader.readLine());
                System.out.println("b = ");
                b = Integer.parseInt(reader.readLine());
                System.out.println("c = ");
                c = Integer.parseInt(reader.readLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Not a number, please try again");
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
        QuadraticEquation.quadr(a, b, c);
    }
}
