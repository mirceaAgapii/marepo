package md.mircea.project.student;

public class Address {
	private String name;
	private int number;
	private String city;
	
	public Address() {
		
	}
	
	public Address(String name, int number, String city) {
		super();
		this.name = name;
		this.number = number;
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Address [name=" + name + ", number=" + number + ", city=" + city + "]";
	}
	
	
	
	

}
