package md.mircea.project.student;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context =
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Student student = context.getBean("student", Student.class);
		Student student2 = context.getBean("student2", Student.class);
		
		context.close();
		
		System.out.println(student);
		System.out.println(student2);
	}
}
