package md.mircea.project.student;

import java.util.List;
import java.util.Map;

public class Student {
	public int id;
	private String name;
	private int age;
	
	private Address address;
	private List<String> friends;
	
	private Map<String, String> bestFriends;
	

	public Student() {
		
	}
	
	public Student(int id, String name, int age, Map<String, String> bestFriends) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.bestFriends = bestFriends;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public List<String> getFriends() {
		return friends;
	}

	public void setFriends(List<String> friends) {
		this.friends = friends;
	}
	
	public Map<String, String> getBestFriends() {
		return bestFriends;
	}

	public void setBestFriends(Map<String, String> bestFriends) {
		this.bestFriends = bestFriends;
	}


	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", address=" + address + ", friends=" + friends
				+ ", bestFriends=" + bestFriends + "]";
	}


	
}
