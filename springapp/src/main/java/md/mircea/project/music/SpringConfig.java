package md.mircea.project.music;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan(basePackages = "md.mircea.project.music")
public class SpringConfig {

//    @Bean
//    public MusicPlayer musicPlayer() {
//        return new MusicPlayer(getMusic());
//    }

    @Bean
    public ClassicalMusic clasicalMusic() {
        return new ClassicalMusic();
    }

    @Bean
    public RockMusic rockMusic() {
        return new RockMusic();
    }

    @Bean
    public MetalMusic metalMusic() {
        return new MetalMusic();
    }

    public List<Music> getMusic() {
        List<Music> music = new ArrayList<>();
        music.add(clasicalMusic());
        music.add(rockMusic());
        music.add(metalMusic());
        return music;
    }
}
