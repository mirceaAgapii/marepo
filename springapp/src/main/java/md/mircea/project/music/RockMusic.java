package md.mircea.project.music;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class RockMusic implements Music {

	private static List<String> songs = 
			new ArrayList<>(Arrays.asList("Linkin Park - In The End",
										  "TFK  - War of Change",
										  "Billy Talent - Rusted from the rain"));
	
	@Override
	public String play() {
		int random = (int) Math.floor(Math.random()*3);
		return "Playing rock song: " + songs.get(random);
	}

}
