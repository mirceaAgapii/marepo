package md.mircea.project.music;

public enum MusicTypes {
	CLASSICAL_MUSIC,
	ROCK_MUSIC,
	METAL_MUSIC
}