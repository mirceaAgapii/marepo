package md.mircea.project.music;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ClassicalMusic implements Music {

	private static List<String> songs = 
			new ArrayList<>(Arrays.asList("Hungarian Rhapsody", "Moonlight Sonata", "Fur Elise"));
	
	@Override
	public String play() {
		int random = (int) Math.floor(Math.random()*3);
		return "Playing classical song: " + songs.get(random);
	}
}
