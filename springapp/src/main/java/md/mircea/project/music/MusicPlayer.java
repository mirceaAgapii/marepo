package md.mircea.project.music;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class MusicPlayer {
	private List<Music> music;
	
	@Autowired
    public MusicPlayer(@Qualifier("rockMusic") Music music1,
                       @Qualifier("classicalMusic") Music music2,
                       @Qualifier("metalMusic") Music music3) {
        this.music = new ArrayList<>(Arrays.asList(music1, music2, music3));
    }

//	public MusicPlayer(List<Music> music) {
//		this.music = music;
//	}
	
//	public void playMusic(MusicTypes type) {
//		if(type == MusicTypes.ROCK_MUSIC) {
//			System.out.println(music2.play());
//		} else if (type == MusicTypes.CLASSICAL_MUSIC) {
//			System.out.println(music1.play());
//		}
//	}
	
	public void playMusic(MusicTypes type) {
		if(type == MusicTypes.ROCK_MUSIC) {
			System.out.println(music.get(0).play());
		} else if (type == MusicTypes.CLASSICAL_MUSIC) {
			System.out.println(music.get(1).play());
		} else {
            System.out.println(music.get(2).play());
        }
	}

	public void playMusic() {
		System.out.println(music.get((int)Math.floor(Math.random() * 3)).play());
	}
}
