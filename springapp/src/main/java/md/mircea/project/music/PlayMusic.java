package md.mircea.project.music;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PlayMusic {
	
	public static void main(String[] args) {
//		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

		MusicPlayer player = context.getBean(MusicPlayer.class);
	
//		player.playMusic(MusicTypes.ROCK_MUSIC);
//		player.playMusic(MusicTypes.CLASSICAL_MUSIC);


		System.out.println("Playing random songs of every genre:");
		player.playMusic(MusicTypes.ROCK_MUSIC);
		player.playMusic(MusicTypes.CLASSICAL_MUSIC);
		player.playMusic(MusicTypes.METAL_MUSIC);

		System.out.println("\nPlaying random songs:");
		for (int i = 0; i < 5; i++) {
			player.playMusic();
		}

		context.close();
	}

}
