package md.mircea.project.music;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class MetalMusic implements Music {
    private static List<String> songs =
            new ArrayList<>(Arrays.asList("Slipknot - Psychosocial",
                                          "Manowar - Die for Metal",
                                          "AC/DC - Thunderstruck"));

    @Override
    public String play() {
        int random = (int) Math.floor(Math.random()*3);
        return "Playing metal song: " + songs.get(random);
    }
}
