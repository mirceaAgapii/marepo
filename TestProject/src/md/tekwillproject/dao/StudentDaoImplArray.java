/*
 * Here comes the text of your license
 * Each line should be prefixed with  * 
 */
package md.tekwillproject.dao;

import md.tekwillproject.model.Student;
import org.springframework.stereotype.Component;
/**
 *
 * @author Syros
 */
@Component
public class StudentDaoImplArray implements StudentDao {

/**
 * Using an array as database
 * @author Syros
 */

    private Student studDb[] = new Student[20];

    /* Adding students to DB */
    public StudentDaoImplArray() {
        Student stud0 = new Student("Rodica", "Gutu", 19, "FB-101");
        studDb[0] = stud0;
        Student stud1 = new Student("Mircea", "Agapii", 23, "CON-101");
        studDb[1] = stud1;
        Student stud2 = new Student("Ion", "Morari", 22, "CON-102");
        studDb[2] = stud2;
        Student stud3 = new Student("Vasile", "Petrescu", 22, "CON-102");
        studDb[3] = stud3;
        Student stud4 = new Student("Erica", "Cuflic", 20, "MK-101");
        studDb[4] = stud4;
        Student stud5 = new Student("Mariana", "Cret", 20, "MK-101");
        studDb[5] = stud5;
        Student stud6 = new Student("Andrei", "Botnari", 21, "CIE-101");
        studDb[6] = stud6;
        Student stud7 = new Student("Tatiana", "Goncearu", 22, "CON-102");
        studDb[7] = stud7;
        Student stud8 = new Student("Oleg", "Berzoi", 22, "CON-102");
        studDb[8] = stud8;
        Student stud9 = new Student("Liliana", "Smochina", 24, "CON-101");
        studDb[9] = stud9;
        Student stud10 = new Student("Aurelia", "Birca", 23, "CON-101");
        studDb[10] = stud10;
        Student stud11 = new Student("Vitalie", "Birca", 24, "CON-101");
        studDb[11] = stud11;
        Student stud12 = new Student("Alina", "Secara", 20, "MK-101");
        studDb[12] = stud12;
        Student stud13 = new Student("Victoria", "Agapii", 21, "CIE-101");
        studDb[13] = stud13;
    }


    /*
    * Getting index of first null object in array
    */
    @Override
    public int getIndexOfFirstNull() {
        for(int i = 0; i < studDb.length; i++) {
            if(studDb[i] == null) {
                return i;
            }
        }
        return studDb.length;
    }

    /*
    * Adding data to array
    */
    @Override
    public void add(Student stud, int index) {
        studDb[index] = stud;
    }

    /*
    * Removing data from array
    */
    @Override
    public void remove(int id) {
        for (int i = 0; i < studDb.length; i++) {
            if (studDb[i].getId() == id) {
                studDb[i] = null;
                break;
            }            
        }

        /*reordering sudents in array to prevent null data between objects*/
        for (int i = 0; i < studDb.length - 1; i++) {
            if(studDb[i] == null) {
                studDb[i] = studDb[i + 1];
                studDb[i + 1] = null;
            }
        }
    }

    /*
    * Updating data in objects from array
    */
    @Override
    public void update(int index, String name, String surname,
                       int age, String group) {
        studDb[index].setName(name);
        studDb[index].setSurname(surname);
        studDb[index].setAge(age);
        studDb[index].setGroup(group);
    }

    /*Getters*/
    @Override
    public int getID(int index) {
        return studDb[index].getId();
    }
    
    @Override
    public String getName(int index) {
        return studDb[index].getName();
    }

    @Override
    public String getSurname(int index) {
        return studDb[index].getSurname();
    }

    @Override
    public int getAge(int index) {
        return studDb[index].getAge();
    }

    @Override
    public String getGroup(int index) {
        return studDb[index].getGroup();
    }
    
    @Override
    public Student getStudent(int index) {
        return studDb[index];
    }

    /*Shows the content of array in terminal, used for testing*/
    @Override
    public void showDb() {
        for(Student stud: studDb) {
            if(stud != null) {
                System.out.println(stud);
            }
        }
        System.out.println();
    }
}
