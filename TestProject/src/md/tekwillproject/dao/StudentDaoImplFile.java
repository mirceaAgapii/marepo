package md.tekwillproject.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import md.tekwillproject.model.Student;

/**
 *
 * @author Syros
 */
public class StudentDaoImplFile implements StudentDao {

    private static final File FILE = new File("studDB.txt");

    private final List<Student> STUDENT_LIST = new ArrayList<>();
    
    private List<Student> readFromFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE))) {

            STUDENT_LIST.clear();

            String line = "";

            while ((line = reader.readLine()) != null) {
                String[] studInfoArr = line.split(" ");
                
                Student student = new Student(0);
                
                student.setId(Integer.parseInt(studInfoArr[0]));
                student.setName(studInfoArr[1]);
                student.setSurname(studInfoArr[2]);
                student.setAge(Integer.parseInt(studInfoArr[3]));
                student.setGroup(studInfoArr[4]);

                STUDENT_LIST.add(student);
            }
        } catch (IOException e) {
            System.out.println("Exception in readFromFile meth " + e);
        }
        return STUDENT_LIST;
    }
    
    private void writeToFile(List<Student> stud) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE))) {


            for (Student student : stud) {
                writer.write(student.getId() + 
                        " " + student.getName() + 
                        " " + student.getSurname() +
                        " " + student.getAge() + 
                        " " + student.getGroup());
                writer.newLine();
            }

        } catch (IOException e) {
            System.out.println("Exception in writeToFile meth " + e);
        }
    }
        
    private void checkFileIfExists() {
        try {
            if (!FILE.exists()) {
                FILE.createNewFile();
            }
        } catch (IOException e) {
            System.out.println("Exception in add meth: file not found " + e);
        }
    }

    @Override
    public int getIndexOfFirstNull() {
        this.checkFileIfExists();
        this.readFromFile();

        return STUDENT_LIST.size();
    }
    
    
    
    @Override
    public void add(Student stud, int index) {
        this.checkFileIfExists();
        this.readFromFile();

        STUDENT_LIST.add(index, stud);

        this.writeToFile(STUDENT_LIST);
    }

    @Override
    public void remove(int id) {
        this.checkFileIfExists();
        this.readFromFile();

        Iterator iterator = STUDENT_LIST.iterator();
        
        while(iterator.hasNext()) {
            if (((Student) iterator.next()).getId() == id) {
                iterator.remove();
                break;
            }
        }
        this.writeToFile(STUDENT_LIST);
    }

    @Override
    public void update(int id, String name, String surname, int age, String group) {
        this.checkFileIfExists();
        this.readFromFile();
        
        for (Student student : STUDENT_LIST) {
            if (student.getId() == id) {
                student.setName(name);
                student.setSurname(surname);
                student.setAge(age);
                student.setGroup(group);
                break;
            }
        }
        this.writeToFile(STUDENT_LIST);
    }

    @Override
    public int getID(int index) {
        return STUDENT_LIST.get(index).getId();
    }
    
    @Override
    public String getName(int index) {
        return STUDENT_LIST.get(index).getName();
    }

    @Override
    public String getSurname(int index) {
        return STUDENT_LIST.get(index).getSurname();
    }

    @Override
    public int getAge(int index) {
        return STUDENT_LIST.get(index).getAge();
    }

    @Override
    public String getGroup(int index) {
        return STUDENT_LIST.get(index).getGroup();
    }

    @Override
    public Student getStudent(int index) {
        return STUDENT_LIST.get(index);
    }

    @Override
    public void showDb() {
        this.checkFileIfExists();
        this.readFromFile();

        STUDENT_LIST.forEach(System.out::println);
    }
    
    public int getCount() {
        this.checkFileIfExists();
        this.readFromFile();
        
        if (STUDENT_LIST.size() == 0)
            return 1;
        
        return STUDENT_LIST.get(STUDENT_LIST.size() - 1).getId() + 1;
    }
}
