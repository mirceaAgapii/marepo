package md.tekwillproject.dao;

import java.util.Map;
import md.tekwillproject.model.Student;

/**
 * Interface of DAO for Student data base
 * 
 * @author Syros
 */
public interface StudentDao {
    
    /*
    * Getting index of first null object in array
    */
    public int getIndexOfFirstNull();
     
    /*
    * Adding data to DB
    */
    public void add(Student stud, int index);
     
    /*
    * Removing data from DB
    */
    public void remove(int id);
     
    /*
    * Update data in DB
    */
    public void update(int id, String name, String surname,
            int age, String group);
     
    /**
    * Getters
    *
    * @param index of student in db
    * @return id of student obj
    */
    public int getID(int index);
     
    /**
    *
    * @param index of student in db
    * @return name of student obj
    */
    public String getName(int index);
     
    /**
    * 
    * @param index of student in db
    * @return surname of student obj
    */
    public String getSurname(int index);
     
    /**
    * 
    * @param index of student in db
    * @return age of student obj
    */
    public int getAge(int index);

    /**
    * 
    * @param index of student in db
    * @return group of student obj
    */
    public String getGroup(int index);
     
    /**
    * 
    * @param index of student in db
    * @return student obj
    */
    public Student getStudent(int index);
     
    /**
    * getCount method is used for db based on arrays like Array and ArrayList
    * to get the order of students for creatind unique id's
    * 
    * @return static count from Students class
    */
    public default int getCount() {
        return Student.getCount();
    }
    
    /*
    * Shows the content of array in terminal, used for testing
    */
    public void showDb();
    
    
    
    default void setMathGrades(Map<String,Integer> grades, int id) {
        
    }
    default void setPhysGrades(Map<String,Integer> grades, int id) {
        
    }
    default void setCSGrades(Map<String,Integer> grades, int id) {
        
    }
    
    default Map<String, Integer> getMathGrades(int index) {
        return null;
    }
    default Map<String, Integer> getPhysGrades(int index) {
        return null;
    }
    default Map<String, Integer> getCSGrades(int index) {
        return null;
    }
}
