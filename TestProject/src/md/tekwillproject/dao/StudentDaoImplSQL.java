package md.tekwillproject.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import md.tekwillproject.model.Student;
import org.springframework.stereotype.Component;

/**
 * if there are problems with connection to db from getConnection below
 * set time zone in mysql terminal
 * 
 *             SET GLOBAL time_zone = '+2:00';

 * @author Syros
 */
@Component
public class StudentDaoImplSQL implements StudentDao {
    private static final String URL = "jdbc:mysql://localhost:3306/tekwill_academy_db?zeroDateTimeBehavior=convertToNull&useSSL=false";
    private static final String USER = "root";
    private static final String PASS = "root";
    
    private Connection connection;
    private Statement statement;
    
    private void getConnection() {
        try {
            connection = DriverManager.getConnection(URL, USER, PASS);
            statement = (Statement) connection.createStatement();
        } catch (SQLException e) {
            System.out.println("Can't connect to DBin StudentDao, see javadoc");
        }
    }
    
    private void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("SQLException when trying to close connection");
        }
    }

    /**
    * Getting index of last element in table
    * @return numberOfRows
     */
    @Override
    public int getIndexOfFirstNull() {
        int numberOfRows = 0;
        
        getConnection();
        try {
            ResultSet result = statement.executeQuery("SELECT count(*) FROM students;");

            result.next();
            numberOfRows = result.getInt("count(*)");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("SQLException in getIndexOfLast in StudentDao");
        } finally {
            closeConnection();
        }
        return numberOfRows;
    }

    /*
    * Adding data to db
     */
    @Override
    public void add(Student stud, int index) {
        getConnection();
        try {
            String name = stud.getName();
            String surname = stud.getSurname();
            int age = stud.getAge();
            String group = stud.getGroup();
            
            String insert = "INSERT INTO students (name, surname, age, group_name)"
                    + " VALUES ('"+name+"','"+surname+"','"+age+"','"+group+"')";
            statement.executeUpdate(insert);
            
            
            ResultSet result = statement.executeQuery("SELECT id FROM students LIMIT " + index + ",1;");
            result.next();             
            int id = result.getInt("id");
            
            insert = "INSERT INTO student_math_grades (student_id) VALUES ('"+ id +"');";
            statement.executeUpdate(insert);
            
            insert = "INSERT INTO student_phys_grades (student_id) VALUES ('"+ id +"');";
            statement.executeUpdate(insert);
            
            insert = "INSERT INTO student_cs_grades (student_id) VALUES ('"+ id +"');";
            statement.executeUpdate(insert);
            
            System.out.println(name + " was inserted in db");
            
        } catch (SQLException e) {
            System.out.println("SQLException in add Method in StudentDao" + e);
        } finally {
            closeConnection();
        }
    }

    /*
    * Removing data from db 
    */
    @Override
    public void remove(int id) {
        getConnection();
        try {
            String delete = "DELETE FROM students WHERE id = " + id + ";";
            statement.executeUpdate(delete);
            
            delete = "DELETE FROM student_math_grades WHERE student_id = " + id + ";";
            statement.executeUpdate(delete);
            
            System.out.println("Student was deleted from db");
            
        } catch (SQLException e) {
            System.out.println("SQLException in add Method in StudentDao");
        } finally {
            closeConnection();
        }
    }

    /*
    * Updating data in db
    */
    @Override
    public void update(int id, String name, String surname,
            int age, String group) {
        getConnection();
        try {
            String update = "UPDATE students SET name = '"+name+"', surname = "
                    + "'"+surname+"', age = '"+age+"', group_name = '"+group+"' "
                    + "WHERE id = " + id + ";";
            statement.executeUpdate(update);
            
            System.out.println("Student with id = " + id + " was update in db");
            
        } catch (SQLException e) {
            System.out.println("SQLException in update Method in StudentDao");
        } finally {
            closeConnection();
        }
    }

    /*Getters*/
    @Override
    public int getID(int index) {
        int id = 0;
        
        getConnection();
        try {
            ResultSet result = statement.executeQuery("SELECT id FROM students LIMIT " + index + ",1;");
            
            result.next();             
            id = result.getInt("id");
            
        } catch (SQLException e) {
            System.out.println("SQLException in getId meth StudentDao");
        } finally {
            closeConnection();
        }
        return id;
    }

    @Override
    public String getName(int index) {
        String name = "";
        
        getConnection();
        try  {         
            ResultSet result = statement.executeQuery("SELECT name FROM students LIMIT " + index + ",1;");
            
            result.next();             
            name = result.getString("name");
            
        } catch (SQLException e) {
            System.out.println("SQLException in getName meth StudentDao");
        } finally {
            closeConnection();
        }
        return name;
    }

    @Override
    public String getSurname(int index) {
        String surname = "";
        getConnection();
        try {
            ResultSet result = statement.executeQuery("SELECT surname FROM students LIMIT " + index + ",1;");
            
            result.next();             
            surname = result.getString("surname");
            
        } catch (SQLException e) {
            System.out.println("SQLException in getSurname meth StudentDao");
        } finally {
            closeConnection();
        }
        return surname;
    }

    @Override
    public int getAge(int index) {
        int age = 0;
        
        getConnection();
        try {          
            ResultSet result = statement.executeQuery("SELECT age FROM students LIMIT " + index + ",1;");
            
            result.next();             
            age = result.getInt("age");
            
        } catch (SQLException e) {
            System.out.println("SQLException in getAge meth StudentDao");
        } finally {
            closeConnection();
        }
        return age;
    }
    
    @Override
    public String getGroup(int index) {
        String group = "";
        
        getConnection();
        try {
            ResultSet result = statement.executeQuery("SELECT group_name FROM students LIMIT " + index + ",1;");
            
            result.next();             
            group = result.getString("group_name");
            
        } catch (SQLException e) {
            System.out.println("SQLException in getGroup meth StudentDao");
        } finally {
            closeConnection();
        }
        return group;
    }

    @Override
    public Student getStudent(int index) {
        Student stud = new Student();
        
        getConnection();
         try {            
            ResultSet result = statement.executeQuery("SELECT * FROM students LIMIT " + index + ",1;");
            
            while (result.next()) {
                stud.setId(result.getInt("id"));
                stud.setName(result.getString("name"));
                stud.setSurname(result.getString("surname"));
                stud.setAge(result.getInt("age"));
                stud.setGroup(result.getString("group_name"));
                
            }
        } catch (SQLException e) {
            System.out.println("SQLException in showDb meth StudentDao");
        } finally {
            closeConnection();
        }
        return stud;
    }

    /*Shows the content of DB in terminal, used for testing*/
    @Override
    public void showDb() {
        getConnection();
        try {
            ResultSet result = statement.executeQuery("SELECT * FROM students;");
            
            while (result.next()) {
                Student stud = new Student();
                
                stud.setId(result.getInt("id"));
                stud.setName(result.getString("name"));
                stud.setSurname(result.getString("surname"));
                stud.setAge(result.getInt("age"));
                stud.setGroup(result.getString("group_name"));
                System.out.println(stud);
            }
        } catch (SQLException e) {
            System.out.println("SQLException in showDb meth StudentDao");
        } finally {
            closeConnection();
        }
    }
    
    @Override
    public void setMathGrades(Map<String,Integer> grades, int id) {
        Map<String, Integer> map = new HashMap<>();
        map.putAll(grades);
        
        getConnection();
        try {

            String update = "UPDATE student_math_grades SET sept = '" + map.get("September") + 
                    "', oct = '" + map.get("October") + "', nov = '" + map.get("November") + 
                    "', dece = '" + map.get("December") + "', jan = '" + map.get("January") + 
                    "', feb  = '" + map.get("February") + "', mar = '" + map.get("March") + 
                    "', apr = '" + map.get("April") + "', may = '" + map.get("May") + "' "+ 
                    "WHERE student_id = " + id + ";";
            statement.executeUpdate(update);
            
            
        } catch (SQLException e) {
            System.out.println("SQLException in getMathGrades Method in StudentDao" + e);
        } finally {
            closeConnection();
        }
    }
    public void setPhysGrades(Map<String,Integer> grades, int id) {
        Map<String, Integer> map = new HashMap<>();
        map.putAll(grades);
        
        getConnection();
        try {

            String update = "UPDATE student_phys_grades SET sept = '" + map.get("September") + 
                    "', oct = '" + map.get("October") + "', nov = '" + map.get("November") + 
                    "', dece = '" + map.get("December") + "', jan = '" + map.get("January") + 
                    "', feb  = '" + map.get("February") + "', mar = '" + map.get("March") + 
                    "', apr = '" + map.get("April") + "', may = '" + map.get("May") + "' "+ 
                    "WHERE student_id = " + id + ";";
            statement.executeUpdate(update);
            
            
        } catch (SQLException e) {
            System.out.println("SQLException in getPhysGrades Method in StudentDao" + e);
        } finally {
            closeConnection();
        }
    }
    public void setCSGrades(Map<String,Integer> grades, int id) {
        Map<String, Integer> map = new HashMap<>();
        map.putAll(grades);
        
        getConnection();
        try {

            String update = "UPDATE student_cs_grades SET sept = '" + map.get("September") + 
                    "', oct = '" + map.get("October") + "', nov = '" + map.get("November") + 
                    "', dece = '" + map.get("December") + "', jan = '" + map.get("January") + 
                    "', feb  = '" + map.get("February") + "', mar = '" + map.get("March") + 
                    "', apr = '" + map.get("April") + "', may = '" + map.get("May") + "' "+ 
                    "WHERE student_id = " + id + ";";
            statement.executeUpdate(update);
            
            
        } catch (SQLException e) {
            System.out.println("SQLException in getCSGrades Method in StudentDao" + e);
        } finally {
            closeConnection();
        }
    }
    
    public Map<String, Integer> getMathGrades(int index) {
        Map<String, Integer> mathGrades = new HashMap<>();
        
        getConnection();
         try {            
            ResultSet result = statement.executeQuery("SELECT * FROM student_math_grades LIMIT " + index + ",1;");
            
            while (result.next()) {
                mathGrades.put("September", result.getInt("sept"));
                mathGrades.put("October", result.getInt("oct"));
                mathGrades.put("November", result.getInt("nov"));
                mathGrades.put("December", result.getInt("dece"));
                mathGrades.put("January", result.getInt("jan"));
                mathGrades.put("February", result.getInt("feb"));
                mathGrades.put("March", result.getInt("mar"));
                mathGrades.put("April", result.getInt("apr"));
                mathGrades.put("May", result.getInt("may"));
 
            }
        } catch (SQLException e) {
            System.out.println("SQLException in getMathGrades meth StudentDao: " + e);
        } finally {
            closeConnection();
        }
        return mathGrades;
    }
    public Map<String, Integer> getPhysGrades(int index) {
        Map<String, Integer> physGrades = new HashMap<>();
        
        getConnection();
         try {            
            ResultSet result = statement.executeQuery("SELECT * FROM student_phys_grades LIMIT " + index + ",1;");
            
            while (result.next()) {
                physGrades.put("September", result.getInt("sept"));
                physGrades.put("October", result.getInt("oct"));
                physGrades.put("November", result.getInt("nov"));
                physGrades.put("December", result.getInt("dece"));
                physGrades.put("January", result.getInt("jan"));
                physGrades.put("February", result.getInt("feb"));
                physGrades.put("March", result.getInt("mar"));
                physGrades.put("April", result.getInt("apr"));
                physGrades.put("May", result.getInt("may"));
 
            }
        } catch (SQLException e) {
            System.out.println("SQLException in getPhysGrades meth StudentDao: " + e);
        } finally {
            closeConnection();
        }
        return physGrades;
    }
    public Map<String, Integer> getCSGrades(int index) {
        Map<String, Integer> scGrades = new HashMap<>();
        
        getConnection();
         try {            
            ResultSet result = statement.executeQuery("SELECT * FROM student_cs_grades LIMIT " + index + ",1;");
            
            while (result.next()) {
                scGrades.put("September", result.getInt("sept"));
                scGrades.put("October", result.getInt("oct"));
                scGrades.put("November", result.getInt("nov"));
                scGrades.put("December", result.getInt("dece"));
                scGrades.put("January", result.getInt("jan"));
                scGrades.put("February", result.getInt("feb"));
                scGrades.put("March", result.getInt("mar"));
                scGrades.put("April", result.getInt("apr"));
                scGrades.put("May", result.getInt("may"));
 
            }
        } catch (SQLException e) {
            System.out.println("SQLException in getCSGrades meth StudentDao: " + e);
        } finally {
            closeConnection();
        }
        return scGrades;
    }
    
}