package md.tekwillproject.dao;

import java.util.ArrayList;
import java.util.List;
import md.tekwillproject.model.User;

/**
 *
 * @author Syros
 */
public class UserDao {

    private List<User> userDB = new ArrayList<>();
    private User connectedUser;

    /* Adding users to DB */
    public UserDao() {
        User admin = new User("admin", "admin", "admin");
        userDB.add(admin);

        User decan = new User("decan", "decan", "read/write");
        userDB.add(decan);

        User secretar = new User("secretar", "secretar", "read/write");
        userDB.add(secretar);

        User prof1 = new User("prof_math", "math", "read only");
        userDB.add(prof1);

        User prof2 = new User("prof_fiz", "fiz", "read only");
        userDB.add(prof2);

        User prof3 = new User("prof_cs", "cs", "read only");
        userDB.add(prof3);
    }

    /* Check users */
    public boolean checkUser(String name, String pass) {

        for (User user : userDB) {
            if (name.equals(user.getUserName()) && pass.equals(user.getPassword())) {
                this.connectedUser = user;
                return true;
            }
        }
        return false;
    }

    public boolean checkAccess() {

        if (connectedUser.getLevelOfAcces().equals("admin")
                || connectedUser.getLevelOfAcces().equals("read/write")) {
            return true;
        }
        return false;
    }
}
