package md.tekwillproject.dao;

import java.util.ArrayList;
import java.util.List;
import md.tekwillproject.model.Group;
import md.tekwillproject.model.Student;

/**
 *
 * @author Syros
 */
public class GroupDao {

    private List<Group> groupDB = new ArrayList<>();

    /* Constructor */
    public GroupDao() {
        Group groupCon101 = new Group("CON-101", "Lisnic Victoria");
        groupDB.add(groupCon101);

        Group groupCon102 = new Group("CON-102", "Gutu Tatiana");
        groupDB.add(groupCon102);

        Group groupMk101 = new Group("MK-101", "Mereuta Victor");
        groupDB.add(groupMk101);

        Group groupMk102 = new Group("MK-102", "Brad Gheorghe");
        groupDB.add(groupMk102);

        Group groupFb101 = new Group("FB-101", "Matiet Veaceslav");
        groupDB.add(groupFb101);

        Group groupFb102 = new Group("FB-102", "Oprea Tatiana");
        groupDB.add(groupFb102);

        Group groupCie101 = new Group("CIE-101", "Cernei Elizaveta");
        groupDB.add(groupCie101);
    }

    /* Getters */
    public String getGroupMaster(String groupName) {
        for (Group group : groupDB) {
            if (group.getGroupName().equals(groupName)) {
                return group.getGroupMaster();
            }
        }
        return "Group master not set";
    }

    public void showDB(String groupName) {
        for (Group group : groupDB) {
            if (group.getGroupName().equals(groupName)) {
                System.out.println(group);
            }
        }
        System.out.println();
    }

    public List<Student> getStudentList(String groupName) {
        for (Group group : groupDB) {
            if (group.getGroupName().equals(groupName)) {
                return group.getStudents();
            }
        }
        return null;
    }

    public void updateStudentList() {
        for (Group g : groupDB) {
            g.setStudents(g.getGroupName());
        }
    }
}
