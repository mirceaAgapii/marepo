package md.tekwillproject.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import md.tekwillproject.model.Student;
import org.springframework.stereotype.Component;

/**
 *
 * @author Syros
 */
@Component
public class StudentDaoImplSer implements StudentDao {

    private static final File FILE = new File("stud.ser");
    
    private final Map<Integer, Student> STUDENT_MAP = new HashMap<>();
    
    /*Serialization*/
    private void serialize(Map<Integer, Student> studentMap) {
        
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILE))) {
            studentMap.forEach((k , s) -> {
                try {
                    oos.writeObject(s);
                } catch (IOException ex) {
                    System.out.println("IOException" + ex);
                }
            });
            System.out.println("Serialized with succes");
        } catch (IOException e) {
            System.out.println("File not found " + e);
        } 

    }
    
    private void deserialize() {
        STUDENT_MAP.clear();
        Student stud;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(FILE);
            ObjectInputStream ois = new ObjectInputStream(fis);

            while (fis.available() > 0) {
                stud = (Student) ois.readObject();
                STUDENT_MAP.put(stud.getId(), stud);
            }

        } catch (IOException e) {
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            System.out.println("Deserialize " +  e);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                
            }
        }


    }
    
    private boolean checkIfFileExists() {
        if(FILE != null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int getIndexOfFirstNull() {
        if(checkIfFileExists()) {
            this.deserialize();
            return STUDENT_MAP.size();
        }
        return 1;
    }

    @Override
    public void add(Student stud, int index) {
        
        this.deserialize();
        STUDENT_MAP.put(index + 1, stud);
        this.serialize(STUDENT_MAP);
    }

    @Override
    public void remove(int id) {
        this.deserialize();
        STUDENT_MAP.remove(id);
        this.serialize(STUDENT_MAP);
    }

    @Override
    public void update(int id, String name, String surname, int age, String group) {
        this.deserialize();
        STUDENT_MAP.forEach((k, s) -> {
            if(k == id) {
                s.setName(name);
                s.setSurname(surname);
                s.setAge(age);
                s.setGroup(group);
            }
        });
        this.serialize(STUDENT_MAP);
    }

    @Override
    public int getID(int index) {
        return STUDENT_MAP.get(index + 1).getId();
    }

    @Override
    public String getName(int index) {
        return STUDENT_MAP.get(index + 1).getName();
    }

    @Override
    public String getSurname(int index) {
        return STUDENT_MAP.get(index + 1).getSurname();
    }

    @Override
    public int getAge(int index) {
        return STUDENT_MAP.get(index + 1).getAge();
    }

    @Override
    public String getGroup(int index) {
        return STUDENT_MAP.get(index + 1).getGroup();
    }

    @Override
    public Student getStudent(int index) {
        return STUDENT_MAP.get(index);
    }

    @Override
    public void showDb() {
        this.deserialize();
        STUDENT_MAP.forEach((k, s) -> System.out.println(s));
    }
    
    @Override
    public int getCount() {
        this.deserialize();
        if(STUDENT_MAP.size() == 0) {
            return 1;
        }

        int max = Collections.max(STUDENT_MAP.keySet());
        
        return max + 1;
    }
    
}
