package md.tekwillproject.dao;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import md.tekwillproject.model.Student;
import org.springframework.stereotype.Component;

/**
 * New version of static DB on ArrayList
 *
 * @author Syros
 */
@Component
public class StudentDaoImplList implements StudentDao {

    private List<Student> studDb = new ArrayList<>();

    /* Adding students to DB */
    public StudentDaoImplList() {
        Student stud0 = new Student("Rodica", "Gutu", 19, "FB-101");
        studDb.add(stud0);
        
        Student stud1 = new Student("Mircea", "Agapii", 23, "CON-101");
        studDb.add(stud1);
        
        Student stud2 = new Student("Ion", "Morari", 22, "CON-102");
        studDb.add(stud2);
        
        Student stud3 = new Student("Vasile", "Petrescu", 22, "CON-102");
        studDb.add(stud3);
        
        Student stud4 = new Student("Erica", "Cuflic", 20, "MK-101");
        studDb.add(stud4);
        
        Student stud5 = new Student("Mariana", "Cret", 20, "MK-101");
        studDb.add(stud5);
        
        Student stud6 = new Student("Andrei", "Botnari", 21, "CIE-101");
        studDb.add(stud6);
        
        Student stud7 = new Student("Tatiana", "Goncearu", 22, "FB-102");
        studDb.add(stud7);
        
        Student stud8 = new Student("Oleg", "Berzoi", 22, "CON-102");
        studDb.add(stud8);
        
        Student stud9 = new Student("Liliana", "Smochina", 24, "CON-101");
        studDb.add(stud9);
        
        Student stud10 = new Student("Aurelia", "Birca", 23, "CON-101");
        studDb.add(stud10);
        
        Student stud11 = new Student("Vitalie", "Birca", 24, "MK-102");
        studDb.add(stud11);
        
        Student stud12 = new Student("Alina", "Secara", 20, "MK-101");
        studDb.add(stud12);
        
        Student stud13 = new Student("Victoria", "Agapii", 21, "CIE-101");
        studDb.add(stud13);
    }


    /*
    * Getting index of first null object in array
    */
    @Override
    public int getIndexOfFirstNull() {
        return studDb.size();
    }

    public int getIndex(int id) {
        for(int i = 0; i < studDb.size(); i++) {
            if(studDb.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    /*
    * Adding data to array
    */
    @Override
    public void add(Student stud, int index) {
        studDb.add(index, stud);
    }

    /*
    * Removing data from array
    */
    @Override
    public void remove(int id) {
        Iterator iterator = studDb.iterator();
        
        while(iterator.hasNext()) {
            if (((Student) iterator.next()).getId() == id) {
                iterator.remove();
                break;
            }
        }
    }

    /*
    * Updating data in objects from array
    */
    @Override
    public void update(int id, String name, String surname,
                       int age, String group) {
        
        for (Student student : studDb) {
            if (student.getId() == id) {
                student.setName(name);
                student.setSurname(surname);
                student.setAge(age);
                student.setGroup(group);
                break;
            }
        }
    }

    /*Getters*/
    @Override
    public int getID(int index) {
        return studDb.get(index).getId();
    }

    @Override
    public String getName(int index) {
        return studDb.get(index).getName();
    }

    @Override
    public String getSurname(int index) {
        return studDb.get(index).getSurname();
    }

    @Override
    public int getAge(int index) {
        return studDb.get(index).getAge();
    }

    @Override
    public String getGroup(int index) {
        return studDb.get(index).getGroup();
    }

    @Override
    public Student getStudent(int index) {
        return studDb.get(index);
    }
    
    @Override
    public int getCount() {
        return Student.getCount();
    }

    /**
    * Shows the content of array in terminal, used for testing
    */
    @Override
    public void showDb() {
        for(Student stud: studDb) {
            if(stud != null) {
                System.out.println(stud);
            }
        }
        System.out.println();
    }
}

