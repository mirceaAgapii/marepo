package md.tekwillproject.services;

import java.util.Map;
import md.tekwillproject.dao.StudentDao;
import md.tekwillproject.model.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * CRUD functions
 * @author Syros
 */
public class StudentServices {
    private static final ApplicationContext CONTEXT = new ClassPathXmlApplicationContext("spring.xml");
    private static StudentServices instance = null;
    private static final StudentDao DAO = CONTEXT.getBean("studentDao", StudentDao.class);
    
    /*
    * Private constructor used for creating only one instance of StudentServices
    * class, acording to Singleton Pattern
    */
    private StudentServices() {
        
    }
    
    public static StudentServices getInstance() {
        if (instance == null) {
            instance = new StudentServices();
        }
        return instance;
    }
    
    public int getIndexOfFirstNull() {
        return DAO.getIndexOfFirstNull();
    }
    
    /*
    * Adding data to db (Create)
    */
    public void add(Student stud, int index) {
        DAO.add(stud, index);
    }
    
    /*
    * Removing data from db (Delete)
    */
    public void remove(int id) { 
        DAO.remove(id);
    }
    
    /*
    * Updating data in db (Update)
    */
    public void update(int id, String name, String surname, 
                       int age, String group) {
        DAO.update(id, name, surname, age, group);
        
    }
    
    public void setMathGrades(Map<String,Integer> grades, int id) {
        DAO.setMathGrades(grades, id);
    }
    public void setPhysGrades(Map<String,Integer> grades, int id) {
        DAO.setPhysGrades(grades, id);
    }
    public void setCSGrades(Map<String,Integer> grades, int id) {
        DAO.setCSGrades(grades, id);
    }
    
    /*
    * Getting data from db (Read)
    */
    public int getID(int index) {
        return DAO.getID(index);
    }
    
    public String getName(int index) {
        return DAO.getName(index);
    }
    
    public String getSurname(int index) {
        return DAO.getSurname(index);
    }
    
    public int getAge(int index) {
        return DAO.getAge(index);
    }
    
    public String getGroup(int index) {
        return DAO.getGroup(index);
    }
    
    public Student getStudent(int index) {
        return DAO.getStudent(index);
    }
    
    public Map<String, Integer> getMathGrades(int index) {
        return DAO.getMathGrades(index);
    }
    public Map<String, Integer> getPhysGrades(int index) {
        return DAO.getPhysGrades(index);
    }
    public Map<String, Integer> getCSGrades(int index) {
        return DAO.getCSGrades(index);
    }
    
    /*
    * Get the count of students
    */
    public int getCount() {
        return DAO.getCount();
    }
    
    /*
    * Printing to terminal data from DB
    * For testing
    */
    public void showDb() {
        DAO.showDb();
    }
    
}
