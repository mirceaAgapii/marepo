package md.tekwillproject.services;

import java.util.List;
import md.tekwillproject.dao.GroupDao;
import md.tekwillproject.model.Student;

/**
 *
 * @author Syros
 */
public class GroupServices {
    private static GroupDao dao = new GroupDao();
    
    public String getGroupMaster(String groupName) {
        return dao.getGroupMaster(groupName);
    }
    
    public void showDB(String group) {
        dao.showDB(group);
    }
    
    public List<Student> getStudentList(String group) {
        return dao.getStudentList(group);
    }
    
    public void updateStudentList() {
        dao.updateStudentList();
    } 
}
