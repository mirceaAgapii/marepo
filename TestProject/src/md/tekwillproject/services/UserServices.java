package md.tekwillproject.services;

import md.tekwillproject.dao.UserDao;

/**
 *
 * @author Syrosd
 */
public class UserServices {
    private static UserDao dao = new UserDao();
    
    public boolean checkUser(String name, String pass) {
        return dao.checkUser(name, pass);
    }
    
    public boolean checkAccess() {
        return dao.checkAccess();
    }
}
