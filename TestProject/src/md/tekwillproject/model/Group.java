package md.tekwillproject.model;

import java.util.ArrayList;
import java.util.List;
import md.tekwillproject.services.StudentServices;

/**
 *
 * @author Syros
 */
public class Group {
    private String groupName;
    private String groupMaster;
    private List<Student> students = new ArrayList<>();
//    private Student students[] = new Student[20];
    
    /* Constructor */
    public Group(String groupName, String groupMaster) {
        this.groupName = groupName;
        this.groupMaster = groupMaster;
        setStudents(groupName);
    }
    
    
    /* Setters */
    public void setGroupName(String name) {
        this.groupName = name;
    }

    public void setGroupMaster(String groupMaster) {
        this.groupMaster = groupMaster;
    }
    
    public void setStudents(String group) {
        students.clear();

        int j = 0;
        
        for(int i = 0; i < StudentServices.getInstance().getIndexOfFirstNull(); i++) {
            if(StudentServices.getInstance().getGroup(i).equals(group)) {
                students.add(StudentServices.getInstance().getStudent(i));
            }
        }
    }
    
    
    /* Getters */

    public String getGroupName() {
        return groupName;
    }

    public String getGroupMaster() {
        return groupMaster;
    }

    public List<Student> getStudents() {
        return students;
    }

    /* Index of the first null element */
    public int getIndex() {
        return students.size();
    }
    
    /* Printing to terminal our DB */
    public void showGroup(String group) {
        System.out.println("Group: " + group);
        
        for(Student stud: students) {
            System.out.println(stud);
        }
    }
    
    @Override
    public String toString() {
        System.out.println("Group{" + "groupName = " + groupName + 
                ", groupMaster = " + groupMaster + ", students = \n{");
        for(Student student: students) {
            if(student != null) {
                System.out.println(student);
            }
        }
        System.out.println('}');

        return "";
    }




    
    
}
