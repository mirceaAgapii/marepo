package md.tekwillproject.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import static md.tekwillproject.services.StudentServices.*;


/**
 *
 * @author Syros
 */
public class Student implements Serializable {
    private int id;
    private String name;
    private String surname;
    private int age;
    private String group;
    private final transient Map<String, Integer> MATH_GRADES = new HashMap<>();
    private final transient Map<String, Integer> PHYS_GRADES = new HashMap<>();
    private final transient Map<String, Integer> CS_GRADES = new HashMap<>();
    
    /**
     * static count of the created students, used to
     * asign unique id for every new student 
     */
    private static int count;

    public Student() {
        count++;
    }
    
    /**
    * This constructor is used to create instances of Student in StudentDaoImplFile 
    * class without incrementing count variable, becouse those instances are doubles for
    * objects created in StudentRegistrationForm
    */
    public Student(int i) {
       
    }
    
    public Student(int id, String name, String surname, int age, String group) {
        count++;
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.group = group; 
    }
    
    public Student(String name, String surname, int age, String group) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.group = group;
        this.id = ++count;
    }
    

    /*Setters*/
    public void setId(int id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGroup(String group) {
        this.group = group;
    }
    
    /*Getters*/
    public int getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public int getAge() {
        return this.age;
    }

    public String getGroup() {
        return this.group;
    }
    
    public static int getCount() {
        return count;
    }
    
    public Map<String, Integer> getMathGrades() {
        return this.MATH_GRADES;
    }
    
    public Map<String, Integer> getPhysGrades() {
        return this.PHYS_GRADES;
    }
    
    public Map<String, Integer> getCSGrades() {
        return this.CS_GRADES;
    }
    
    @Override
    public String toString() {
        return "Student{" + "id=" + id + ", name=" + name + ", surname=" + surname + 
                ", age=" + age + ", group=" + group /*+ ", GRADES=" + MATH_GRADES + '}'*/;
    }
    
}
