package md.tekwillproject.model;

/**
 *
 * @author Syros
 */
public class User {
    private String userName;
    private String password;
    private String levelOfAcces;

    /* Constructor */
    public User(String userName, String password, String levelOfAcces) {
        this.userName = userName;
        this.password = password;
        this.levelOfAcces = levelOfAcces;
    }
    
    
    /* Setters */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLevelOfAcces(String levelOfAcces) {
        this.levelOfAcces = levelOfAcces;
    }

    /* Getters */
    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getLevelOfAcces() {
        return levelOfAcces;
    }
    
    
    
}
