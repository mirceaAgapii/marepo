package md.mycompany.springboottest.service;

import java.util.List;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SupplierService implements ServiceInterface<Supplier> {

    @Autowired
    @Qualifier("supplierDaoImplJpa")
    private DaoInterface supplierDao;

    @Override
    public boolean add(Supplier supplier) {
        return supplierDao.add(supplier);
    }

    @Override
    public void update(Supplier supplier) {
        supplierDao.update(supplier);
    }

    @Override
    public void delete(Supplier supplier) {
        supplierDao.delete(supplier);
    }

    @Override
    public List<Supplier> getAll() {
        return supplierDao.getAll();
    }

}
