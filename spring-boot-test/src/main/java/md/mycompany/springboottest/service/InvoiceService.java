package md.mycompany.springboottest.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.model.Invoice;
import md.mycompany.springboottest.model.InvoiceProducts;
import md.mycompany.springboottest.model.Product;
import md.mycompany.springboottest.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class InvoiceService implements ServiceInterface<Invoice> {

    private DaoInterface dao;
    private ServiceInterface productService;
    private ServiceInterface supplierService;
    
    @Autowired
    public InvoiceService(@Qualifier("invoiceDaoImpl") DaoInterface dao, 
                          @Qualifier("productService") ServiceInterface productService, 
                          @Qualifier("supplierService") ServiceInterface supplierService) {
        this.dao = dao;
        this.productService = productService;
        this.supplierService = supplierService;
    }

    
    @Override
    public boolean add(Invoice invoice) {
        //Adding data about new invoice in db
        dao.add(invoice);
       
        //Adding new products in db and updating existing ones
        
        //Getting a set of all products from invoice with prices and quantities
        Set<InvoiceProducts> products = invoice.getProducts();
        
        //Getting a list of products from db
        List<Product> allProducts = productService.getAll();
        
        //Getting a list of suppliers from db
        List<Supplier> allSuppliers = supplierService.getAll();
        
        //Streaming through Set to add or update every product
        products.forEach(ip -> {
            
            //Temporary product from Set
            Product tempProudct = ip.getProduct();
            tempProudct.setPrice(ip.getPrice());
            tempProudct.setQuantity(ip.getQuantity());
            
            //Trying to add all products to db
            //If a product does not exist in db, it will be added and the rest 
            //of the code will not be executed,
            //else - will update the existing product with 
            //new data(quatity and overall price)
            if (!productService.add(tempProudct)) {
                
                //Getting the coresponding product from list(db)
                Optional<Product> optionalProduct = allProducts.stream()
                        .filter(p -> p.equals(tempProudct)).findFirst();
                
                //Checking if in Optional is an object
                if (optionalProduct.isPresent()) {
                    
                    //Old data from db
                    double oldPrice = optionalProduct.get().getPrice();
                    double oldQuantity = optionalProduct.get().getQuantity();
                    
                    //New data from Set
                    double newPrice = tempProudct.getPrice();
                    double newQuantity = tempProudct.getQuantity();
                    
                    tempProudct.setQuantity(newQuantity + oldQuantity);
                    tempProudct.setPrice(( (oldPrice * oldQuantity) + (newPrice * newQuantity) ) 
                                                / (oldQuantity + newQuantity) );
                    
                    productService.update(tempProudct);
                }
            }
        });
        
        //Adding new supplier(true) else updating existing one(false)
        if (!supplierService.add(invoice.getSupplier())) {
            double newDebt = invoice.getTotalSum();
            //Serching for supplier to be updated our_debt (old + new)
            allSuppliers.forEach(s -> {
                double currentDebt = s.getOurDebt();
                if(s.equals(invoice.getSupplier())) {
                    s.setOurDebt(currentDebt + newDebt);
                    supplierService.update(s);
                }
            });
        }
        
        return true;
    }

    @Override
    public void update(Invoice invoice) {
        
    }

    @Override
    public void delete(Invoice invoice) {
        
    }

    @Override
    public List<Invoice> getAll() {
        return dao.getAll();
    }

}
