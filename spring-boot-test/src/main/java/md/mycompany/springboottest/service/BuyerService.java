package md.mycompany.springboottest.service;

import java.util.List;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.model.Buyer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class BuyerService implements ServiceInterface<Buyer> {

    private final DaoInterface BUYER_DAO;

    @Autowired
    public BuyerService(@Qualifier("buyerDaoImplJpa") DaoInterface buyerDao) {
        this.BUYER_DAO = buyerDao;
    }
    
    

    @Override
    public boolean add(Buyer buyer) {
        return BUYER_DAO.add(buyer);
    }

    @Override
    public void update(Buyer buyer) {
        BUYER_DAO.update(buyer);
    }

    @Override
    public void delete(Buyer buyer) {
        BUYER_DAO.delete(buyer);
    }

    @Override
    public List<Buyer> getAll() {
        return BUYER_DAO.getAll();
    }

}
