package md.mycompany.springboottest.service;

import java.util.List;
import md.mycompany.springboottest.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import md.mycompany.springboottest.dao.DaoInterface;
import org.springframework.stereotype.Service;

@Service
public class ProductService implements ServiceInterface<Product> {

    private final DaoInterface PRODUCT_DAO;

    @Autowired
    public ProductService(@Qualifier("productDaoImplJpa") DaoInterface productDao) {
        this.PRODUCT_DAO = productDao;
    }
    
    
    public boolean add(Product product) {
        return PRODUCT_DAO.add(product);
    }

    public void update(Product product) {
        PRODUCT_DAO.update(product);
    }

    public void delete(Product product) {
        PRODUCT_DAO.delete(product);
    }

    public List<Product> getAll() {
        return PRODUCT_DAO.getAll();
    }
}
