package md.mycompany.springboottest.service;

import java.util.List;

public interface ServiceInterface<T> {

    boolean add(T t);

    void update(T t);

    void delete(T t);

    List<T> getAll();
}
