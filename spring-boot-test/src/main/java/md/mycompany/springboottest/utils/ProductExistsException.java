package md.mycompany.springboottest.utils;

public class ProductExistsException extends Exception {
    public ProductExistsException(String msg) {
        super(msg);
    }
}
