package md.mycompany.springboottest.controller;

import java.util.List;

public interface ControllerInterface<T> {

    void add(T t);

    void update(T t);

    default void delete(int id){
        
    }

    T getByID(int id);

    List<T> getAll();
}
