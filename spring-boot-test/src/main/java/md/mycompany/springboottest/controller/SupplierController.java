package md.mycompany.springboottest.controller;

import java.util.List;
import java.util.stream.Collectors;
import md.mycompany.springboottest.model.Supplier;
import md.mycompany.springboottest.service.ServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/suppliers")
public class SupplierController implements ControllerInterface<Supplier> {

    @Autowired
    @Qualifier("supplierService")
    private ServiceInterface service;

    @Override
    @PostMapping
    public void add(@RequestBody Supplier supplier) {
        service.add(supplier);
    }

    @Override
    @PutMapping
    public void update(@RequestBody Supplier supplier) {
        service.update(supplier);
    }

    @Override
    @DeleteMapping("{id}")
    public void delete(@PathVariable int id) {
        service.delete(this.getByID(id));
    }

    @Override
    @GetMapping(params = {"id"})
    public Supplier getByID(int id) {
        List<Supplier> list = service.getAll();
        Supplier supplier = null;
        if (list.stream().anyMatch(s -> s.getId() == id)) {
            List<Supplier> collect = list.stream().filter(s -> s.getId() == id)
                    .collect(Collectors.toList());
            supplier = collect.get(0);
        }
        return supplier;
    }

    @Override
    @GetMapping
    public List<Supplier> getAll() {
        return service.getAll();
    }
}
