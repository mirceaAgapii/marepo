package md.mycompany.springboottest.controller;

import java.util.List;
import java.util.stream.Collectors;
import md.mycompany.springboottest.model.Product;
import md.mycompany.springboottest.service.ServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
public class ProductController implements ControllerInterface<Product> {

    @Autowired
    @Qualifier("productService")
    private ServiceInterface service;

    @Override
    @PostMapping
    public void add(@RequestBody Product product) {
        service.add(product);
    }

    @Override
    @PutMapping
    public void update(@RequestBody Product product) {
        service.update(product);
    }

    @Override
    @DeleteMapping("{id}")
    public void delete(@PathVariable int id) {
        service.delete(this.getByID(id));
    }

    @Override
    @GetMapping(params = {"id"})
    public Product getByID(int id) {
        List<Product> list = service.getAll();
        Product product = null;
        if (list.stream().anyMatch(p -> p.getId() == id)) {
            List<Product> collect = list.stream().filter(p -> p.getId() == id)
                    .collect(Collectors.toList());

            product = collect.get(0);
        }
        return product;
    }

    @Override
    @GetMapping
    public List<Product> getAll() {
        return service.getAll();
    }
}
