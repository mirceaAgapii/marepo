package md.mycompany.springboottest.controller;

import java.util.List;
import md.mycompany.springboottest.model.Invoice;
import md.mycompany.springboottest.service.ServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/invoice")
public class InvoiceController implements ControllerInterface<Invoice> {
    
    @Autowired
    @Qualifier("invoiceService")
    private ServiceInterface service;

    @Override
    @PostMapping
    public void add(@RequestBody Invoice invoice) {
        service.add(invoice);
    }

    @Override
    public void update(@RequestBody Invoice invoice) {
        System.out.println("Tipa updating...");
    }

    @DeleteMapping("/{serialNumber}")
    public void delete(@PathVariable String serialNumber) {
        System.out.println("Tipa deleting...");
    }

    @Override
    public Invoice getByID(int id) {
        System.out.println("Tipa getById");
        return null;
    }

    @Override
    @GetMapping
    public List<Invoice> getAll() {
        return service.getAll();
    }
    
    
}
