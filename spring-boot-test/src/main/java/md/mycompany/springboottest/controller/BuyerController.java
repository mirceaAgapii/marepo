package md.mycompany.springboottest.controller;

import java.util.List;
import java.util.stream.Collectors;
import md.mycompany.springboottest.model.Buyer;
import md.mycompany.springboottest.service.ServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/buyers")
public class BuyerController implements ControllerInterface<Buyer> {

    @Autowired
    @Qualifier("buyerService")
    private ServiceInterface service;

    @Override
    @PostMapping
    public void add(@RequestBody Buyer buyer) {
        service.add(buyer);
    }

    @Override
    @PutMapping
    public void update(@RequestBody Buyer buyer) {
        service.update(buyer);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        service.delete(this.getByID(id));
    }

    @Override
    @GetMapping(params = {"id"})
    public Buyer getByID(int id) {
        List<Buyer> buyers = service.getAll();
        Buyer buyer = null;

        if (buyers.stream().anyMatch(b -> b.getId() == id)) {
            List<Buyer> collect = buyers.stream().filter(b -> b.getId() == id)
                    .collect(Collectors.toList());
            buyer = collect.get(0);
        }
        return buyer;
    }

    @Override
    @GetMapping
    public List<Buyer> getAll() {
        return service.getAll();
    }

}
