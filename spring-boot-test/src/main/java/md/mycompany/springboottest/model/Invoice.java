package md.mycompany.springboottest.model;

import java.time.LocalDate;
import java.util.Set;

public class Invoice {
    private String serialNumber;
    private LocalDate date;
    private Supplier supplier;
    private Set<InvoiceProducts> products;
    private double totalSum;
    
    public Invoice() {
    }

    public Invoice(String serialNumber, LocalDate date, Supplier supplier, Set<InvoiceProducts> products) {
        this.serialNumber = serialNumber;
        this.date = date;
        this.supplier = supplier;
        this.products = products;
    }
    
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Set<InvoiceProducts> getProducts() {
        return products;
    }

    public void setProducts(Set<InvoiceProducts> products) {
        this.products = products;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    @Override
    public String toString() {
        return "Invoice{" + "serialNumber=" + serialNumber + ", date=" + date + ", supplier=" + supplier + ", products=" + products + ", totalSum=" + totalSum + '}';
    }
    
    
}
