package md.mycompany.springboottest.model;

public class InvoiceProducts {
    private Product product;
    private int quantity;
    private double price;
    private transient double sum;

    public InvoiceProducts() {
    }

    public InvoiceProducts(Product product, int quantity, double price) {
        this.product = product;
        this.quantity = quantity;
        this.price = price;
        this.sum = this.price * this.quantity;
    }
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSum() {
        return sum;
    }


    @Override
    public String toString() {
        return "InvoiceProducts{" + "product=" + product + ", quantity=" + quantity + ", price=" + price + ", sum=" + sum + '}';
    }
    
    
}
