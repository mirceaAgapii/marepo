package md.mycompany.springboottest.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import md.mycompany.springboottest.model.Product;
import org.springframework.stereotype.Component;

/**
 *
 * @author Syros
 */
@Component
public class ProductDaoImpl implements DaoInterface<Product> {

    private static Map<Integer, Product> productMap = new HashMap<>();

    @Override
    public boolean add(Product product) {
        productMap.put(product.getId(), product);
        return true;
    }

    @Override
    public boolean update(Product product) {
        productMap.replace(product.getId(), product);
        return true;
    }

    @Override
    public boolean delete(Product product) {
        productMap.remove(product.getId());
        return true;
    }

    @Override
    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        productMap.forEach((k, v) -> products.add(v));
        return products;
    }

}
