package md.mycompany.springboottest.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import md.mycompany.springboottest.model.Invoice;
import md.mycompany.springboottest.model.InvoiceProducts;
import md.mycompany.springboottest.model.Product;
import md.mycompany.springboottest.model.Supplier;
import org.springframework.stereotype.Component;

@Component
public class InvoiceDaoImpl implements DaoInterface<Invoice> {

    private static Map<String, Invoice> invoiceMap = new HashMap<>();

    static {
        Set<InvoiceProducts> set = new HashSet<>();
        Set<InvoiceProducts> set1 = new HashSet<>();
        Set<InvoiceProducts> set2 = new HashSet<>();
        InvoiceProducts invoiceProducts1 = new InvoiceProducts(new Product("FirstProd"), 150, 20);
        InvoiceProducts invoiceProducts2 = new InvoiceProducts(new Product("SecondProd"), 350, 25);
        InvoiceProducts invoiceProducts3 = new InvoiceProducts(new Product("ThirdProd"), 50, 30);
        InvoiceProducts invoiceProducts4 = new InvoiceProducts(new Product("FourthProd"), 450, 50);
        
        set.add(invoiceProducts1);
        set.add(invoiceProducts2);
        set1.add(invoiceProducts3);
        set2.add(invoiceProducts4);
        
        
        Invoice invoice1 = new Invoice("AAA0000001", LocalDate.of(2019, 11, 30), 
                new Supplier("Some Suplier", "Some country", 0), set);
        Invoice invoice2 = new Invoice("AAA0000002", LocalDate.of(2019, 12, 02), 
                new Supplier("Some Suplier2", "Some country2", 0), set1);
        Invoice invoice3 = new Invoice("AAA0000003", LocalDate.of(2019, 12, 02), 
                new Supplier("Some Suplier3", "Some country3", 0), set2);
        
        invoiceMap.put(invoice1.getSerialNumber(), invoice1);
        invoiceMap.put(invoice2.getSerialNumber(), invoice2);
        invoiceMap.put(invoice3.getSerialNumber(), invoice3);
    }
    
    @Override
    public boolean add(Invoice invoice) {
        invoiceMap.put(invoice.getSerialNumber(), invoice);
        return true;
    }

    @Override
    public boolean update(Invoice invoice) {
        invoiceMap.replace(invoice.getSerialNumber(), invoice);
        return true;
    }

    @Override
    public boolean delete(Invoice invoice) {
        System.out.println("Cant delete for now");
        return true;
    }

    @Override
    public List<Invoice> getAll() {
        List<Invoice> invoiceList = new ArrayList<>();
        invoiceMap.forEach((k, v) -> invoiceList.add(v));
        return invoiceList;
    }

}
