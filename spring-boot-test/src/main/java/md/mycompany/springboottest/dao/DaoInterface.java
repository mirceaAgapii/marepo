package md.mycompany.springboottest.dao;

import java.util.List;

public interface DaoInterface<T> {

    boolean add(T t);

    boolean update(T t);

    boolean delete(T t);

    List<T> getAll();
}
