package md.mycompany.springboottest.dao.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.dao.JdbcConnenction;
import md.mycompany.springboottest.model.Supplier;
import org.springframework.stereotype.Repository;

@Repository
public class SupplierDaoImplSql implements DaoInterface<Supplier> {

    private static PreparedStatement statment;

    private static final String ADD_SUPPLIER_TO_DB = "INSERT INTO suppliers (name, country) VALUES (?, ?);";
    private static final String UPDATE_SUPPLIER_BY_ID = "UPDATE suppliers SET name=?, country=? WHERE id=?;";
    private static final String DELETE_SUPPLIER_BY_ID = "DELETE FROM suppliers WHERE id=?;";
    private static final String GET_ALL_SUPPLIERS = "SELECT * FROM suppliers";

    @Override
    public boolean add(Supplier suppliers) {
        List<Supplier> list = this.getAll();

        if (!list.stream().anyMatch(p -> p.getName().equals(suppliers.getName()))) {

            try {
                statment = JdbcConnenction.getConnection().prepareStatement(ADD_SUPPLIER_TO_DB);

                statment.setString(1, suppliers.getName());
                statment.setString(2, suppliers.getCountry());

                statment.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    statment.close();
                    JdbcConnenction.closeConnection();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return true;
        } else {
            System.out.println("This product already exists");
            return false;
        }
    }

    @Override
    public boolean update(Supplier suppliers) {
        try {
            statment = JdbcConnenction.getConnection().prepareStatement(UPDATE_SUPPLIER_BY_ID);

            statment.setString(1, suppliers.getName());
            statment.setString(2, suppliers.getCountry());
            statment.setInt(3, suppliers.getId());

            statment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statment.close();
                JdbcConnenction.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean delete(Supplier suppliers) {
        try {
            statment = JdbcConnenction.getConnection().prepareStatement(DELETE_SUPPLIER_BY_ID);

            statment.setInt(1, suppliers.getId());

            statment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statment.close();
                JdbcConnenction.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public List<Supplier> getAll() {
        List<Supplier> suppliers = new ArrayList<>();
        try {
            statment = JdbcConnenction.getConnection().prepareStatement(GET_ALL_SUPPLIERS);

            ResultSet result = statment.executeQuery();

            while (result.next()) {
                Supplier s = new Supplier();
                s.setId(result.getInt("id"));
                s.setName(result.getString("name"));
                s.setCountry(result.getString("country"));
                s.setOurDebt(result.getDouble("our_debt"));
                suppliers.add(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statment.close();
                JdbcConnenction.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return suppliers;
    }

}
