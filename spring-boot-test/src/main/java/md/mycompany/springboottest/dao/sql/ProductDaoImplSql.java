package md.mycompany.springboottest.dao.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.dao.JdbcConnenction;
import md.mycompany.springboottest.model.Product;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDaoImplSql implements DaoInterface<Product> {

    private static PreparedStatement statment;

    private static final String ADD_PRODUCT_TO_DB = "INSERT INTO products (name, price, quantity) VALUES (?, ?, ?);";
    private static final String UPDATE_PRODUCT_BY_ID = "UPDATE products SET name=?, price=?, quantity=? WHERE id=?;";
    private static final String DELETE_PRODUCT_BY_ID = "DELETE FROM products WHERE id=?;";
    private static final String GET_ALL_PRODUCTS = "SELECT * FROM products";

    @Override
    public boolean add(Product product) {
        List<Product> list = this.getAll();

        if (!list.stream().anyMatch(p -> p.getName().equals(product.getName()))) {

            try {
                statment = JdbcConnenction.getConnection().prepareStatement(ADD_PRODUCT_TO_DB);

                statment.setString(1, product.getName());
                statment.setDouble(2, product.getPrice());
                statment.setDouble(3, product.getQuantity());

                statment.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    statment.close();
                    JdbcConnenction.closeConnection();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return true;
        } else {
            System.out.println("This product already exists");
            return false;
        }
    }

    @Override
    public boolean update(Product product) {
        try {
            statment = JdbcConnenction.getConnection().prepareStatement(UPDATE_PRODUCT_BY_ID);

            statment.setString(1, product.getName());
            statment.setDouble(2, product.getPrice());
            statment.setDouble(3, product.getQuantity());
            statment.setInt(4, product.getId());

            statment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statment.close();
                JdbcConnenction.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean delete(Product product) {
        try {
            statment = JdbcConnenction.getConnection().prepareStatement(DELETE_PRODUCT_BY_ID);

            statment.setInt(1, product.getId());

            statment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statment.close();
                JdbcConnenction.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        try {
            statment = JdbcConnenction.getConnection().prepareStatement(GET_ALL_PRODUCTS);

            ResultSet result = statment.executeQuery();

            while (result.next()) {
                Product p = new Product();
                p.setId(result.getInt("id"));
                p.setName(result.getString("name"));
                p.setPrice(result.getDouble("price"));
                p.setQuantity(result.getDouble("quantity"));
                products.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statment.close();
                JdbcConnenction.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return products;
    }
}
