package md.mycompany.springboottest.dao.jpa;

import java.util.ArrayList;
import java.util.List;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.model.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

@Repository
public class SupplierDaoImplJpa implements DaoInterface<Supplier> {
    
    private static final SessionFactory FACTORY = new Configuration()
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(Supplier.class)
            .buildSessionFactory();

    @Override
    public boolean add(Supplier supplier) {
        List<Supplier> list = this.getAll();
        if (!list.stream().anyMatch(s -> s.equals(supplier))) {
            try (Session session = FACTORY.getCurrentSession()) {
                session.beginTransaction();

                session.save(supplier);

                session.getTransaction().commit();
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean update(Supplier supplier) {
        try (Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();

            Supplier tempSupplier = session.get(Supplier.class, supplier.getId());

            tempSupplier.setName(supplier.getName());
            tempSupplier.setCountry(supplier.getCountry());
            tempSupplier.setOurDebt(supplier.getOurDebt());

            session.getTransaction().commit();
        }
        return true;
    }

    @Override
    public boolean delete(Supplier supplier) {
        try (Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();

            Supplier tempSupplier = session.get(Supplier.class, supplier.getId());
            session.delete(tempSupplier);

            session.getTransaction().commit();
        }
        return true;
    }

    @Override
    public List<Supplier> getAll() {
        List<Supplier> list = new ArrayList<>();
        try(Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();
            
            list = session.createQuery("from Supplier").getResultList();
        }
        return list;
    }
    
}
