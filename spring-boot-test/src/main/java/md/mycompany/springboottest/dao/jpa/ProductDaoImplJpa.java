package md.mycompany.springboottest.dao.jpa;

import java.util.ArrayList;
import java.util.List;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.model.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDaoImplJpa implements DaoInterface<Product> {

    private static final SessionFactory FACTORY = new Configuration()
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(Product.class)
            .buildSessionFactory();

    @Override
    public boolean add(Product product) {
        List<Product> listProducts = this.getAll();
        if (!listProducts.stream().anyMatch(p -> p.equals(product))) {
            try (Session session = FACTORY.getCurrentSession()) {
                session.beginTransaction();

                session.save(product);

                session.getTransaction().commit();
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean update(Product product) {
        try (Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();

            Product tempProd = session.get(Product.class, product.getId());

            tempProd.setName(product.getName());
            tempProd.setPrice(product.getPrice());
            tempProd.setQuantity(product.getQuantity());

            session.getTransaction().commit();
        }
        return true;
    }

    @Override
    public boolean delete(Product product) {
        try (Session session = FACTORY.getCurrentSession()) {

            session.beginTransaction();

            Product tempProd = session.get(Product.class, product.getId());
            session.delete(tempProd);

            session.getTransaction().commit();
        }
        return true;
    }

    @Override
    public List<Product> getAll() {
        List<Product> list = new ArrayList<>();

        try (Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();
            list = session.createQuery("from Product").getResultList();
        }
        return list;
    }
}