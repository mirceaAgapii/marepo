package md.mycompany.springboottest.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcConnenction {

    private static final String URL
            = "jdbc:postgresql://localhost:5432/products_db";
    private static final String LOGIN = "postgres";
    private static final String PASSWORD = "root";

    private static Connection connection;

    public static Connection getConnection() {
        try {
            connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Exception in getConnection()");
            e.printStackTrace();
        }
        return connection;
    }

    public static void closeConnection() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(JdbcConnenction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
