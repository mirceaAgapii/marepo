package md.mycompany.springboottest.dao.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.dao.JdbcConnenction;
import md.mycompany.springboottest.model.Buyer;
import org.springframework.stereotype.Repository;

@Repository
public class BuyerDaoImplSql implements DaoInterface<Buyer> {

    private static PreparedStatement statment;

    private static final String ADD_BUYER_TO_DB = "INSERT INTO buyers (name) VALUES (?);";
    private static final String UPDATE_BUYER_BY_ID = "UPDATE buyers SET name=? WHERE id=?;";
    private static final String DELETE_BUYER_BY_ID = "DELETE FROM buyers WHERE id=?;";
    private static final String GET_ALL_BUYERS = "SELECT * FROM buyers";

    @Override
    public boolean add(Buyer buyer) {
        List<Buyer> list = this.getAll();

        if (!list.stream().anyMatch(p -> p.getName().equals(buyer.getName()))) {

            try {
                statment = JdbcConnenction.getConnection().prepareStatement(ADD_BUYER_TO_DB);

                statment.setString(1, buyer.getName());

                statment.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    statment.close();
                    JdbcConnenction.closeConnection();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return true;
        } else {
            System.out.println("This product already exists");
            return false;
        }
    }

    @Override
    public boolean update(Buyer buyer) {
        try {
            statment = JdbcConnenction.getConnection().prepareStatement(UPDATE_BUYER_BY_ID);

            statment.setString(1, buyer.getName());
            statment.setInt(2, buyer.getId());

            statment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statment.close();
                JdbcConnenction.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean delete(Buyer buyer) {
        try {
            statment = JdbcConnenction.getConnection().prepareStatement(DELETE_BUYER_BY_ID);

            statment.setInt(1, buyer.getId());

            statment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statment.close();
                JdbcConnenction.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public List<Buyer> getAll() {
        List<Buyer> buyer = new ArrayList<>();
        try {
            statment = JdbcConnenction.getConnection().prepareStatement(GET_ALL_BUYERS);

            ResultSet result = statment.executeQuery();

            while (result.next()) {
                Buyer b = new Buyer();
                b.setId(result.getInt("id"));
                b.setName(result.getString("name"));
                b.setDebt(result.getDouble("debt"));
                buyer.add(b);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statment.close();
                JdbcConnenction.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return buyer;
    }

}
