package md.mycompany.springboottest.dao.jpa;

import java.util.ArrayList;
import java.util.List;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.model.Buyer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

@Repository
public class BuyerDaoImplJpa implements DaoInterface<Buyer> {

    private static final SessionFactory FACTORY = new Configuration()
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(Buyer.class)
            .buildSessionFactory();

    @Override
    public boolean add(Buyer buyer) {
        try (Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();
            
            session.save(buyer);
        }
        return true;
    }

    @Override
    public boolean update(Buyer buyer) {
        try (Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();

            Buyer tempBuyer = session.get(Buyer.class, buyer.getId());

            tempBuyer.setName(buyer.getName());
            tempBuyer.setDebt(buyer.getDebt());

            session.getTransaction().commit();
        }
        return true;
    }

    @Override
    public boolean delete(Buyer buyer) {
        try (Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();

            Buyer tempBuyer = session.get(Buyer.class, buyer.getId());
            session.delete(tempBuyer);

            session.getTransaction().commit();
        }
        return true;
    }

    @Override
    public List<Buyer> getAll() {
        List<Buyer> list = new ArrayList<>();
        try (Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();

            list = session.createQuery("from Buyer").getResultList();
        }
        return list;
    }

}
