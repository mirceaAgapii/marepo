package md.mycompany.springboottest.dao.jpa;

import java.util.ArrayList;
import java.util.List;
import md.mycompany.springboottest.dao.DaoInterface;
import md.mycompany.springboottest.model.Invoice;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class InvoiceDaoImplJpa implements DaoInterface<Invoice> {
    
    private static final SessionFactory FACTORY = new Configuration()
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(Invoice.class)
            .buildSessionFactory();

    @Override
    public boolean add(Invoice invoice) {
        try(Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();
            
            session.save(invoice);
        }
        return true;
    }

    @Override
    public boolean update(Invoice invoice) {
        try(Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();
            
            Invoice tempInvoice = session.get(Invoice.class, invoice.getSerialNumber());
            
            tempInvoice.setDate(invoice.getDate());
            tempInvoice.setProducts(invoice.getProducts());
            tempInvoice.setSupplier(invoice.getSupplier());
            tempInvoice.setTotalSum(invoice.getTotalSum());
            
            session.getTransaction().commit();
        }
        return true;
    }

    @Override
    public boolean delete(Invoice invoice) {
        try(Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();
            
            Invoice tempInvoice = session.get(Invoice.class, invoice.getSerialNumber());
            session.delete(tempInvoice);
            
            session.getTransaction().commit();
        }
        return true;
    }

    @Override
    public List<Invoice> getAll() {
        List<Invoice> invoices = new ArrayList<>();
        try(Session session = FACTORY.getCurrentSession()) {
            session.beginTransaction();
            
            invoices = session.createQuery("from Invoice").getResultList();
        }
        return invoices;
    }

}
