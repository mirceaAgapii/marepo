export class ParkingLot {
  id: number;
  name: string;
  isFree: boolean;
  dateTime: Date;
}
