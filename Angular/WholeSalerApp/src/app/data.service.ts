import { Injectable } from '@angular/core';
import { Product } from './Model/Product';
import { Observable, of } from 'rxjs';
import { Buyer } from './Model/Buyer';
import { Supplier } from './Model/Supplier';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { EmitterVisitorContext } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private products: Array<Product>;
  private buyers: Array<Buyer>;
  private suppliers: Array<Supplier>;

  constructor(private http: HttpClient) {
    console.log(environment.restUrl);
  }

  getProducts(): Observable<Array<Product>> {
    return this.http.get<Array<Product>>(environment.restUrl + '/products')
    .pipe(
      map(
        data => data.map(
          product => Product.fromHttp(product)
        )
      )
    );
  }

  editProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(environment.restUrl + '/products', product);
  }

  removeProduct(product: Product): Observable<any> {
    return this.http.delete<Product>(environment.restUrl + '/products/' + product.id);
  }

  addProduct(newProduct: Product): Observable<Product> {
    return this.http.post<Product>(environment.restUrl + '/products', newProduct);
  }

  getBuyers(): Observable<Array<Buyer>> {
    return this.http.get<Array<Buyer>>(environment.restUrl + '/buyers')
    .pipe(
      map(
        data => data.map(
          buyer => Buyer.fromHttp(buyer)
        )
      )
    );
  }

  editBuyer(buyer: Buyer): Observable<Buyer> {
    return this.http.put<Buyer>(environment.restUrl + '/buyers', buyer);
  }

  addBuyer(newBuyer: Buyer): Observable<Buyer> {
    return this.http.post<Buyer>(environment.restUrl + '/buyers', newBuyer);
  }

  removeBuyer(buyer: Buyer): Observable<any> {
    return this.http.delete<Buyer>(environment.restUrl + '/buyers/' + buyer.id);
  }

  getSuppliers(): Observable<Array<Supplier>> {
    return this.http.get<Array<Supplier>>(environment.restUrl + '/suppliers')
    .pipe(
      map(
        data => data.map(
          supplier => Supplier.fromHttp(supplier)
        )
      )
    );
  }

  editSupplier(supplier: Supplier): Observable<Supplier> {
    return this.http.put<Supplier>(environment.restUrl + '/suppliers', supplier);
  }

  removeSupplier(supplier: Supplier): Observable<any> {
    return this.http.delete<Supplier>(environment.restUrl + '/suppliers/' + supplier.id);
  }

  addSupplier(newSupplier: Supplier): Observable<Supplier> {
    return this.http.post<Supplier>(environment.restUrl + '/suppliers', newSupplier);
  }

}
