import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Supplier } from 'src/app/Model/Supplier';
import { DataService } from 'src/app/data.service';
import { Router, ActivationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-supplier-details',
  templateUrl: './supplier-details.component.html',
  styleUrls: ['./supplier-details.component.css']
})
export class SupplierDetailsComponent implements OnInit {

  @Input()
  supplier: Supplier;

  @Output()
  dataChangedEvent = new EventEmitter();

  constructor(private dataService: DataService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }

  editSupplier() {
    this.router.navigate(['admin', 'suppliers'], {queryParams : {id : this.supplier.id, action : 'edit'}});
  }

  removeSupplier() {
    this.dataService.removeSupplier(this.supplier).subscribe(
      () => {
        this.router.navigate(['admin', 'suppliers']);
        this.dataChangedEvent.emit();
      }
    );
  }

}
