import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { Supplier } from 'src/app/Model/Supplier';
import { FormResetService } from 'src/app/form-reset.service';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements OnInit {

  selectedSupplier: Supplier;
  suppliers: Array<Supplier>;
  action: string;

  constructor(private dataService: DataService,
              private router: Router,
              private route: ActivatedRoute,
              private formResetService: FormResetService) { }

  ngOnInit() {
    this.loadData();
  }

  viewDetails(id: number) {
    this.selectedSupplier = this.suppliers.find(p => p.id === id);
    this.router.navigate(['admin', 'suppliers'], {queryParams : {id, action : 'view'}});
  }

  addSupplier() {
    this.loadData();
    this.formResetService.supplierEditResetEvent.emit(this.selectedSupplier);
    this.router.navigate(['admin', 'suppliers'], {queryParams : {action : 'add'}});
  }

  loadData() {
    this.dataService.getSuppliers().subscribe(
      data => this.suppliers = data
    );
    this.processUrlParams();
    this.selectedSupplier = new Supplier();
  }

  processUrlParams() {
    this.route.queryParams.subscribe(
      params => {
        // tslint:disable: no-string-literal
        const id = +params['id'];
        this.action = params['action'];
        if (id) {
          this.selectedSupplier = this.suppliers.find(p => p.id === id);
        }
      }
    );
  }
}
