import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Supplier } from 'src/app/Model/Supplier';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';
import { FormResetService } from 'src/app/form-reset.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-supplier-edit',
  templateUrl: './supplier-edit.component.html',
  styleUrls: ['./supplier-edit.component.css']
})
export class SupplierEditComponent implements OnInit, OnDestroy {

  @Input()
  supplier: Supplier;
  formSupplier: Supplier;

  @Output()
  dataChangedEvent = new EventEmitter();
  supplierFormResetSubscribe: Subscription;

  constructor(private dataService: DataService,
              private router: Router,
              private formResetService: FormResetService) { }

  ngOnInit() {
    this.initializeForm();
    this.supplierFormResetSubscribe = this.formResetService.supplierEditResetEvent.subscribe(
      (supplier) => {
        this.supplier = supplier;
        this.initializeForm();
      }
    );
  }

  initializeForm() {
    this.formSupplier = Object.assign({}, this.supplier);
  }

  ngOnDestroy() {
    this.supplierFormResetSubscribe.unsubscribe();
  }

  onSubmit() {
    if (this.supplier.id != null) {
      this.dataService.editSupplier(this.formSupplier).subscribe(
        () => {
          this.router.navigate(['admin', 'suppliers']);
          this.dataChangedEvent.emit();
        }
      );
    } else {
      this.dataService.addSupplier(this.formSupplier).subscribe(
        () => {
          this.router.navigate(['admin', 'suppliers']);
          this.dataChangedEvent.emit();
        }
      );
    }
  }

}
