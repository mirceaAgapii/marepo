import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Buyer } from 'src/app/Model/Buyer';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';
import { FormResetService } from 'src/app/form-reset.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-buyer-edit',
  templateUrl: './buyer-edit.component.html',
  styleUrls: ['./buyer-edit.component.css']
})
export class BuyerEditComponent implements OnInit, OnDestroy {

  @Input()
  buyer: Buyer;
  formBuyer: Buyer;

  @Output()
  dataChangedEvent = new EventEmitter();

  buyerEditSubscription: Subscription;

  constructor(private dataService: DataService,
              private router: Router,
              private formResetService: FormResetService) { }

  ngOnInit() {
    this.initializeForm();
    this.buyerEditSubscription = this.formResetService.buyerEditResetEvent.subscribe(
      (buyer) => {
        this.buyer = buyer;
        this.initializeForm();
      }
    );
  }

  ngOnDestroy(): void {
    this.buyerEditSubscription.unsubscribe();
  }

  updatebuyer() {
    if (this.formBuyer.id != null) {
      this.dataService.editBuyer(this.formBuyer).subscribe(
        () => {
          this.router.navigate(['admin', 'buyers'])
          this.dataChangedEvent.emit();
        }
      );
    } else {
      this.dataService.addBuyer(this.formBuyer).subscribe(
        () => {
          this.router.navigate(['admin', 'buyers'])
          this.dataChangedEvent.emit();
        }
      );
    }
  }

  initializeForm() {
    this.formBuyer = Object.assign({}, this.buyer);
  }
}
