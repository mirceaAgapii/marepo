import { Component, OnInit } from '@angular/core';
import { Buyer } from 'src/app/Model/Buyer';
import { DataService } from 'src/app/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormResetService } from 'src/app/form-reset.service';

@Component({
  selector: 'app-buyers',
  templateUrl: './buyers.component.html',
  styleUrls: ['./buyers.component.css']
})
export class BuyersComponent implements OnInit {

  buyers: Array<Buyer>;
  selectedBuyer: Buyer;
  action: string;

  constructor(private dataService: DataService,
              private router: Router,
              private route: ActivatedRoute,
              private formResetService: FormResetService) { }

  ngOnInit() {
    this.loadData();
  }

  viewDetails(id: number) {
    this.selectedBuyer = this.buyers.find(p => p.id === id);
    this.router.navigate(['admin', 'buyers'], {queryParams : {id, action : 'view'}});
  }

  loadData() {
    this.dataService.getBuyers().subscribe(
      data => this.buyers = data
    );
    this.processUrlParams();
    this.selectedBuyer = new Buyer();
  }

  processUrlParams() {
    this.route.queryParams.subscribe(
      params => {
        // tslint:disable: no-string-literal
        const id = +params['id'];
        this.action = params['action'];
        if (id) {
          this.selectedBuyer = this.buyers.find(p => p.id === id);
        }
      }
    );
  }

  addBuyer() {
    this.loadData();
    this.formResetService.buyerEditResetEvent.emit(this.selectedBuyer);
    this.router.navigate(['admin', 'buyers'], {queryParams : {action : 'add'}});
  }

}
