import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Buyer } from 'src/app/Model/Buyer';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-buyer-details',
  templateUrl: './buyer-details.component.html',
  styleUrls: ['./buyer-details.component.css']
})
export class BuyerDetailsComponent implements OnInit {

  @Input()
  buyer: Buyer;

  @Output()
  dataChangedEvent = new EventEmitter();

  constructor(private router: Router,
              private dataService: DataService) { }

  ngOnInit() {
  }

  editBuyer() {
    this.router.navigate(['admin', 'buyers'], {queryParams : {id : this.buyer.id, action : 'edit'}});
  }

  removeBuyer() {
    this.dataService.removeBuyer(this.buyer).subscribe(
      () => {
        this.router.navigate(['admin', 'buyers']);
        this.dataChangedEvent.emit();
      }
    );
  }

}
