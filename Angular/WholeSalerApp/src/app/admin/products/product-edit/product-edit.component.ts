import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Product } from 'src/app/Model/Product';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';
import { FormResetService } from 'src/app/form-reset.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit, OnDestroy {

  @Input()
  product: Product;
  formProduct: Product;

  @Output()
  dataChangedEvent = new EventEmitter();

  formResetEventSubscribe: Subscription;

  constructor(private dataService: DataService,
              private router: Router,
              private formResetService: FormResetService) { }

  ngOnInit() {
    this.initializeForm();
    this.formResetEventSubscribe = this.formResetService.productEditResetEvent.subscribe(
      (product) => {
        this.product = product;
        this.initializeForm();
      }
    );
  }

  ngOnDestroy() {
    this.formResetEventSubscribe.unsubscribe();
  }

  initializeForm() {
    this.formProduct = Object.assign({}, this.product);
  }

  onSubmit() {
    if (this.product.id != null) {
      this.dataService.editProduct(this.formProduct).subscribe(
        () => {
          this.router.navigate(['admin', 'products']);
          this.dataChangedEvent.emit();
        }
      );
    } else {
      this.dataService.addProduct(this.formProduct).subscribe(
        () => {
          this.router.navigate(['admin', 'products']);
          this.dataChangedEvent.emit();
        }
      );
    }
  }

}
