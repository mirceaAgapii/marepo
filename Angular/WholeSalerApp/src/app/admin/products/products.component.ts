import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/Model/Product';
import { DataService } from 'src/app/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormResetService } from 'src/app/form-reset.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Array<Product>;
  selectedProduct: Product;
  action: string;

  constructor(private dataService: DataService,
              private router: Router,
              private route: ActivatedRoute,
              private formResetService: FormResetService) { }

  ngOnInit() {
    this.loadData();
  }

  viewDetails(id: number) {
    this.selectedProduct = this.products.find(p => p.id === id);
    this.router.navigate(['admin', 'products'], {queryParams : {id, action : 'view'}});
  }

  addProduct() {
    this.loadData();
    this.formResetService.productEditResetEvent.emit(this.selectedProduct);
    this.router.navigate(['admin', 'products'], {queryParams : {action : 'add'}});
  }

  loadData() {
    this.dataService.getProducts().subscribe(
      data => this.products = data
    );
    this.processUrlParams();
    this.selectedProduct = new Product();
  }

  processUrlParams() {
    this.route.queryParams.subscribe(
      params => {
        // tslint:disable: no-string-literal
        const id = +params['id'];
        this.action = params['action'];
        if (id) {
          this.selectedProduct = this.products.find(p => p.id === id);
        }
      }
    );
  }

}
