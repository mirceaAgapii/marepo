import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/Model/Product';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { FormResetService } from 'src/app/form-reset.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  @Input()
  product: Product;

  @Output()
  dataChangedEvent = new EventEmitter();

  constructor(private router: Router,
              private dataService: DataService) { }

  ngOnInit() {
  }

  editProduct() {
    this.router.navigate(['admin', 'products'], { queryParams : {id : this.product.id, action : 'edit'}});
  }

  removeProduct() {
    this.dataService.removeProduct(this.product).subscribe(
      () => {
        this.router.navigate(['admin', 'products'])
        this.dataChangedEvent.emit();
      }
    );
  }
}
