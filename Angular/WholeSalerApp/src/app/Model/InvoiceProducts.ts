import { Product } from './Product';

export class InvoiceProducts {
    product: Product;
    quantity: number;
    price: number;
    sum: number;
}
