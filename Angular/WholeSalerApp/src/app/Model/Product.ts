export class Product {
  id: number;
  name: string;
  price: number;
  quantity: number;

  static fromHttp(product: Product): Product {
    const newProduct = new Product();
    newProduct.id = product.id;
    newProduct.name = product.name;
    newProduct.price = product.price;
    newProduct.quantity = product.quantity;
    return newProduct;
  }

}
