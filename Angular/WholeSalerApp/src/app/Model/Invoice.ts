import { Supplier } from './Supplier';
import { InvoiceProducts } from './InvoiceProducts';

export class Invice {
    serialNumber: string;
    date: string;
    supplier: Supplier;
    products = new Array<InvoiceProducts>();
    totalSum: number;
}
