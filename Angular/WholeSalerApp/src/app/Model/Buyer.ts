export class Buyer {
    id: number;
    name: string;
    debt: number;

    static fromHttp(buyer: Buyer): Buyer {
        const newBuyer = new Buyer();
        newBuyer.id = buyer.id;
        newBuyer.name = buyer.name;
        newBuyer.debt = buyer.debt;
        return newBuyer;
    }
}