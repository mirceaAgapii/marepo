export class Supplier {
  id: number;
  name: string;
  country: string;
  ourDebt: number;

  static fromHttp(supplier: Supplier): Supplier {
    const newSupplier = new Supplier();
    newSupplier.id = supplier.id;
    newSupplier.name = supplier.name;
    newSupplier.country = supplier.country;
    newSupplier.ourDebt = supplier.ourDebt;
    return newSupplier;
  }
}
