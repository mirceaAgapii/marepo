import { Injectable } from '@angular/core';
import { Product } from './Model/Product';
import { Observable, of } from 'rxjs';
import { Buyer } from './Model/Buyer';
import { Supplier } from './Model/Supplier';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private products: Array<Product>;
  private buyers: Array<Buyer>;
  private suppliers: Array<Supplier>;

  constructor() {
    this.products = new Array<Product>();
    this.buyers = new Array<Buyer>();
    this.suppliers = new Array<Supplier>();

    const firstProd = new Product();
    firstProd.id = 1;
    firstProd.name = 'Apples';
    firstProd.price = 50;
    firstProd.quantity = 20;
    this.products.push(firstProd);

    const secondProd = new Product();
    secondProd.id = 2;
    secondProd.name = 'Strawberry';
    secondProd.price = 150;
    secondProd.quantity = 10;
    this.products.push(secondProd);

    const thirdProd = new Product();
    thirdProd.id = 3;
    thirdProd.name = 'Melon';
    thirdProd.price = 200;
    thirdProd.quantity = 35;
    this.products.push(thirdProd);


    const firstBuyer = new Buyer();
    firstBuyer.id = 1;
    firstBuyer.name = 'Vistarcom SRL';
    firstBuyer.debt = 0;
    this.buyers.push(firstBuyer);

    const secondBuyer = new Buyer();
    secondBuyer.id = 2;
    secondBuyer.name = 'Vladalina SRL';
    secondBuyer.debt = 0;
    this.buyers.push(secondBuyer);

    const thirdBuyer = new Buyer();
    thirdBuyer.id = 3;
    thirdBuyer.name = 'Metro Cash & Carry SRL';
    thirdBuyer.debt = 0;
    this.buyers.push(thirdBuyer);


    const firstSupplier = new Supplier();
    firstSupplier.id = 1;
    firstSupplier.name = 'Macromex SRL';
    firstSupplier.country = 'Romania';
    firstSupplier.ourDebt = 0;
    this.suppliers.push(firstSupplier);

    const secondSupplier = new Supplier();
    secondSupplier.id = 2;
    secondSupplier.name = 'Mlekpol SPA';
    secondSupplier.country = 'Poland';
    secondSupplier.ourDebt = 0;
    this.suppliers.push(secondSupplier);

    const thirdSupplier = new Supplier();
    thirdSupplier.id = 3;
    thirdSupplier.name = 'Makler Komers';
    thirdSupplier.country = 'Bulgary';
    thirdSupplier.ourDebt = 0;
    this.suppliers.push(thirdSupplier);
  }

  getProducts(): Observable<Array<Product>> {
    return of(this.products);
  }

  editProduct(product: Product): Observable<Product> {
    const originalProduct = this.products.find(p => p.id === product.id);
    originalProduct.name = product.name;
    originalProduct.price = product.price;
    originalProduct.quantity = product.quantity;
    return of(originalProduct);
  }

  removeProduct(product: Product): Observable<any> {
    this.products.splice(this.products.indexOf(product), 1);
    return of(null);
  }

  addProduct(newProduct: Product): Observable<Product> {
    let maxId = 0;
    for (const product of this.products) {
      if (product.id > maxId) {
        maxId = product.id;
      }
    }
    newProduct.id = maxId + 1;
    this.products.push(newProduct);
    return of(newProduct);
  }

  getBuyers(): Observable<Array<Buyer>> {
    return of(this.buyers);
  }

  editBuyer(buyer: Buyer): Observable<Buyer> {
    const orignalBuyer = this.buyers.find(b => b.id === buyer.id);
    orignalBuyer.name = buyer.name;
    orignalBuyer.debt = buyer.debt;
    return of(orignalBuyer);
  }

  addBuyer(newBuyer: Buyer): Observable<Buyer> {
    let maxId = 0;
    for (const buyer of this.buyers) {
      if (buyer.id > maxId) {
        maxId = buyer.id;
      }
    }

    newBuyer.id = maxId + 1;
    this.buyers.push(newBuyer);
    return of(newBuyer);
  }

  removeBuyer(buyer: Buyer): Observable<any> {
    this.buyers.splice(this.buyers.indexOf(buyer), 1);
    return of(null);
  }

  getSuppliers(): Observable<Array<Supplier>> {
    return of(this.suppliers);
  }

  editSupplier(supplier: Supplier): Observable<Supplier> {
    const originalSupplier = this.suppliers.find(s => s.id === supplier.id);
    originalSupplier.name = supplier.name;
    originalSupplier.country = supplier.country;
    originalSupplier.ourDebt = supplier.ourDebt;
    return of(originalSupplier);
  }

  removeSupplier(supplier: Supplier): Observable<any> {
    this.suppliers.splice(this.suppliers.indexOf(supplier), 1);
    return of(null);
  }

  addSupplier(newSupplier: Supplier): Observable<Supplier> {
    let maxId = 0;
    for (const supplier of this.suppliers) {
      if (supplier.id > maxId) {
        maxId = supplier.id;
      }
    }
    newSupplier.id = maxId + 1;
    this.suppliers.push(newSupplier);
    return of(newSupplier);
  }

}
