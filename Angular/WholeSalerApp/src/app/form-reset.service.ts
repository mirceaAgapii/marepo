import { Injectable, EventEmitter } from '@angular/core';
import { Buyer } from './Model/Buyer';
import { Product } from './Model/Product';
import { Supplier } from './Model/Supplier';

@Injectable({
  providedIn: 'root'
})
export class FormResetService {

  buyerEditResetEvent = new EventEmitter<Buyer>();
  productEditResetEvent = new EventEmitter<Product>();
  supplierEditResetEvent = new EventEmitter<Supplier>();

  constructor() { }
}
