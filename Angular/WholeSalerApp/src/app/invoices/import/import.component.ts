import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { Product } from 'src/app/Model/Product';
import { InvoiceProducts } from 'src/app/Model/InvoiceProducts';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {

  products: Array<Product>;
  invoiceProducts: Array<InvoiceProducts>;

  total = 100.00;

  invoiceForm: FormGroup;

  constructor(private dataService: DataService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loadData();

    this.invoiceForm = this.formBuilder.group({});

    for (const iProd of this.invoiceProducts) {
      this.invoiceForm.addControl(`prod${iProd.product.name}`, this.formBuilder.control(iProd));
    }
  }

  loadData() {
    this.dataService.getProducts().subscribe(
      data => this.products = data
    );

    this.invoiceProducts = new Array<InvoiceProducts>();
  }

  newElement() {
    if (this.invoiceProducts.length < this.products.length) {
      const newProd = new InvoiceProducts();
      this.invoiceProducts.push(newProd);
    }
  }

}
