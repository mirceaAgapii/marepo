import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToProducts() {
    this.router.navigate(['admin', 'products']);
  }

  navigateToImports() {
    this.router.navigate(['invoice', 'imports']);
  }

  navigateToRetail() {
    this.router.navigate(['invoice', 'retail']);
  }

  navigateToBuyers() {
    this.router.navigate(['admin', 'buyers']);
  }

  navigateToSuppliers() {
    this.router.navigate(['admin', 'suppliers']);
  }

  navigateToMain() {
    this.router.navigate(['']);
  }
}
