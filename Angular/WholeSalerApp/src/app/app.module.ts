import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { MainComponent } from './main/main.component';
import { ProductsComponent } from './admin/products/products.component';
import { BuyersComponent } from './admin/buyers/buyers.component';
import { SuppliersComponent } from './admin/suppliers/suppliers.component';
import { ProductDetailsComponent } from './admin/products/product-details/product-details.component';
import { SupplierDetailsComponent } from './admin/suppliers/supplier-details/supplier-details.component';
import { BuyerDetailsComponent } from './admin/buyers/buyer-details/buyer-details.component';
import { SupplierEditComponent } from './admin/suppliers/supplier-edit/supplier-edit.component';
import { BuyerEditComponent } from './admin/buyers/buyer-edit/buyer-edit.component';
import { ProductEditComponent } from './admin/products/product-edit/product-edit.component';
import { ImportComponent } from './invoices/import/import.component';
import { RetailComponent } from './invoices/retail/retail.component';

const routes: Routes = [
  {path : 'admin/buyers', component : BuyersComponent},
  {path : 'admin/suppliers', component : SuppliersComponent},
  {path : 'admin/products', component : ProductsComponent},
  {path : 'invoice/imports', component : ImportComponent},
  {path : 'invoice/retail', component : RetailComponent},
  {path : '', component : MainComponent},
  {path : '404', component : PageNotFoundComponent},
  {path : '**', redirectTo : '/404'}
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    MenuComponent,
    MainComponent,
    ProductsComponent,
    BuyersComponent,
    SuppliersComponent,
    ProductDetailsComponent,
    SupplierDetailsComponent,
    BuyerDetailsComponent,
    SupplierEditComponent,
    BuyerEditComponent,
    ProductEditComponent,
    ImportComponent,
    RetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
